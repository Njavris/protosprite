#include "InputStack.h"


namespace protosprite
{
namespace input
{
InputStack *InputStack::_instance = nullptr;

InputStack* InputStack::getInstance()
{
    if(_instance == nullptr) _instance = new InputStack();
    return _instance;
};

void InputStack::init()
{
    for (int i = 0; i < KB_KEYS; i++) _keys[i] = false;
    for (int i = 0; i < M_BUTTONS; i++) _mouseButtons[i] = false;
}

}
}
