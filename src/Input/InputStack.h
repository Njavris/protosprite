#pragma once
//#include "../Graphics/GraphicsCalls.h"
#include <list>
#include <cstdlib>
#include <cstring>
#include <array>

#define KB_KEYS 1024
#define M_BUTTONS 16

namespace protosprite
{
namespace input
{

class InputStack
{
    //Window* _window;
    static InputStack *_instance;
    void init();
public:
    std::array<bool, KB_KEYS> _keys;
    std::array<bool, M_BUTTONS> _mouseButtons;
    std::array<double, 2> _mouseXY;
    static InputStack* getInstance();

    void getMouseXY(double &x, double &y) const {x = _mouseXY[0]; y = _mouseXY[1];};
    bool isKbPressed(unsigned int kcode) const {return _keys[kcode];};
    bool isMbPressed(unsigned int button) const {return _mouseButtons[button];};
};
}
}
