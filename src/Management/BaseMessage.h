#pragma once
#include <string>
#include <vector>

#include "Types.h"
#include "GameObject.h"
#include "Attribute.h"

namespace protosprite
{
namespace management
{
class BaseMessage
{
    int _msgDest;
    msgType _msgType;
public:
    BaseMessage(int dest, msgType msgType)
        :_msgDest(dest), _msgType(msgType)
    {};
    const int getDestination() const {return _msgDest;};
    const msgType getMessageType() const {return _msgType;};
    virtual ~BaseMessage() = 0;
};

inline BaseMessage::~BaseMessage() {};

class GenericMessage : public BaseMessage
{
    std::vector<GenericMsgAttribute*> _attrs;
    int _objectId;
    int _sceneId;
    componentType _source;
public:
    GenericMessage(int dest, msgType msgType, componentType src, int scene, int objectId)
        : BaseMessage(dest, msgType), _objectId(objectId), _sceneId(scene), _source(src)
    {};

    ~GenericMessage()
    {
        for (std::vector<GenericMsgAttribute*>::iterator it = _attrs.begin();
             it != _attrs.end();
             it++) {
            GenericMsgAttribute* attr = *it;
            delete attr;
        }
    };

    void addAttribute(attributeType type, std::string name, attributePtr *pAttr)
    {
        _attrs.push_back(new GenericMsgAttribute(type, name, pAttr));
    };

    void addIntAttribute(std::string name, int val)
    {
        _attrs.push_back(new GenericMsgAttribute(attInt, name, new int(val)));
    };

    void addFloatAttribute(std::string name, float val)
    {
        _attrs.push_back(new GenericMsgAttribute(attFloat, name, new float(val)));
    };

    void addStringAttribute(std::string name, std::string value)
    {
        _attrs.push_back(new GenericMsgAttribute(attString, name, new std::string(value)));
    };

    GenericMsgAttribute *getAttribute(std::string attrName)
    {
        int attrNr = findAttribute(attrName);
        if (attrNr != -1)
            return getAttribute(attrNr);
        return nullptr;
    };

    int findAttribute(std::string name)
    {
        for (int i = 0; i < getAttrCount(); i++) {
            GenericMsgAttribute* attr = _attrs[i];
            if (!attr->getAttrName().compare(name))
                return i;
        }
        return -1;
    };

    GenericMsgAttribute *getAttribute(int attrNr) {return _attrs[attrNr];};
    const int getAttrCount() {return _attrs.size();};
    const componentType getSource() {return _source;};
    const int getObjectId() const {return _objectId;};
    const int getSceneId() const {return _sceneId;};
};
}
}
