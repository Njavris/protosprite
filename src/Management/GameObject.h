#pragma once
#include <windows.h>

#include <list>
#include <map>
#include <string>

#include "Types.h"
#include "Attribute.h"
#include "MessageQueue.h"
#include "../Math/Math.h"

namespace protosprite
{
namespace management
{
class GameObject
{
    int _objectId;
    int _sceneId;
    std::list<componentType> _components;
    std::map<std::string, BaseAttribute*> _attributes;
    /* hope I wont need this */
    int _lock;

public:
    GameObject(int objectId, int sceneId);
    ~GameObject();
    void addComponent(componentType comp);
    void removeComponent(componentType comp);
    void addAttribute(std::string name, BaseAttribute *value);
    void removeAttribute(std::string name);
    BaseAttribute *getAttribute(std::string name);
    const int getId() const {return _objectId;};
    const int getSceneId() const {return _sceneId;};
    void lock();
    void unlock();
};
}
}
