#pragma once

#include <iostream>

#include <array>

#include "../Input/InputStack.h"
#include "MessageQueue.h"

namespace protosprite
{
namespace management
{
using namespace input;

class InputObject
{
    input_function _funct;
    int *_keyList;
    int _keyListSz;
    int *_mbList;
    int _mbListSz;
    void *_param;
    int _mouseButton;
public:
    InputObject(int *keyList, int keyListSz, int mb, input_function funct, void *param, int *token);
    ~InputObject();
    bool isPressed();
    void callBack() {(*_funct)(_param);};
    const int *getToken() const {return (int*)this;};
};

/*Should be in same thread as GraphicsManager*/
class InputManager
{
    std::list <InputObject*> _objectList;
    static InputManager *_instance;
    InputManager();
    double _mouseX;
    double _mouseY;
    math::Vec2 _screenScale;
    math::Vec2 _resolution;
public:
    static InputManager *getInstance();
    void setScale(int x, int y) {_screenScale._x = x; _screenScale._y = y;};
    void setResolution(int w, int h) {_resolution._x = w; _resolution._y = h;};
    int addObject(int *keyList, int keyListSz, int isMb, input_function funct, void *param, int *token);
    InputObject *getObject(int *token);
    void getMouseXY(double &x, double &y) const
    {
        x = _mouseX / _screenScale._x;
        y = (_resolution._y - _mouseY) / _screenScale._y;
    };
    void deleteObject(int *token);
    void processInputs();
    void processMessages();
};
}
}
