#pragma once
#include <string>
#include <queue>
#include <vector>
#include <iostream>

#include "BaseMessage.h"

namespace protosprite
{
namespace management
{
class MessageStack
{
    /* One FIFO queue for each component */
    std::vector<std::queue<BaseMessage*>*> _queues;
    static MessageStack *_instance;
    MessageStack() {};
    void initQueues();
public:
    static MessageStack *getInstance();
    void clearQueue(int queueNr);
    BaseMessage *popMessage(int queueNr);
    void pushMessage(BaseMessage *msg, int queueNr);
    void requestAttribute(componentType dst, componentType src, int objId, int sceneId, std::string name);
    void sendAttribute(componentType dst, componentType src, int objId
                       ,int sceneId, std::string name, BaseAttribute* attrib, int change);
};
}
}
