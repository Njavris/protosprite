#pragma once
#include <list>

#include "../Graphics/GraphicsCalls.h"
#include "MessageQueue.h"
#include "SceneManager.h"

namespace protosprite
{
namespace management
{
using namespace graphics;

class GraphicsObject
{
    int _gameObjectId;
    int _sceneId;
    TextGroup* _textGroup; //for now
    int _layerId;
    int _changedFlag;
public:
    GraphicsObject(int sceneId, int gameObjectId)
        :_gameObjectId(gameObjectId), _textGroup(nullptr)
         ,_layerId(0), _changedFlag(1), _sceneId(sceneId) {};
    const int getId() const {return _gameObjectId;};
    const int getSceneId() const {return _sceneId;};
    const int getLayerId() const {return _layerId;};
    TextGroup* getTextGroup() {return _textGroup;};
    void setLayerId(int layerId){_layerId = layerId; setChanged();};
    void setTextGroup(TextGroup *textGroup) {_textGroup = textGroup; setChanged();};
    void setChanged() {_changedFlag = 1;};
    void unsetChanged(){_changedFlag = 0;}
    const int changed() const {return _changedFlag;};
};

class GraphicsManager
{
    std::list <GraphicsObject*> _objectList;
    static GraphicsManager *_instance;
    GraphicsManager() {};
public:
    static GraphicsManager *getInstance();
    void addObject(int sceneId, int objectId);
    void deleteObject(int sceneId, int objectId);
    void processMessages();
    void updateAttribute(int objectId, int sceneId, std::string attributeName, BaseAttribute *value);
    GraphicsObject *getGraphicsObject(int sceneId, int objectId);
    void poll();
};
}
}
