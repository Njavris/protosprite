#include "InputManager.h"

namespace protosprite
{
namespace management
{
InputObject::InputObject(int * keyList, int keyListSz, int mb, input_function funct, void *param, int *token)
    :_keyListSz(keyListSz), _funct(funct), _param(param), _mouseButton(mb)
{
    _keyList = (int*)malloc((keyListSz + 1) * sizeof(int));
    memcpy(_keyList, keyList, (keyListSz + 1) * sizeof(int));
    token = (int*)this;
}

InputObject::~InputObject()
{
    free(_keyList);
}

bool InputObject::isPressed()
{
    InputStack *inputStck = InputStack::getInstance();
    int exec = 0;
    for(int i = 0; i < _keyListSz; i++)
    {
        if( !_mouseButton ?
                _keyList[i] >= 0 && inputStck->isKbPressed(_keyList[i]) :
                _keyList[i] >= 0 && inputStck->isMbPressed(_keyList[i])) {
            exec++;
        }
        else if ( !_mouseButton ?
                 _keyList[i] < 0 && !inputStck->isKbPressed(~_keyList[i]) :
                 _keyList[i] < 0 && !inputStck->isMbPressed(~_keyList[i])) {
            exec++;
        }
    }
    return exec == _keyListSz ? true : false;
}

InputManager *InputManager::_instance = nullptr;

InputManager::InputManager()
{
}

InputManager *InputManager::getInstance()
{
    if (_instance == nullptr)
        _instance = new InputManager();
    return _instance;
}

int InputManager::addObject(int *keyList, int keyListSz, int isMb, input_function funct, void *param, int *token)
{
    _objectList.push_back(new InputObject(keyList, keyListSz, isMb, funct, param, token));
}

void InputManager::deleteObject(int *token)
{
    InputObject *iObj = (InputObject*)token;
    _objectList.remove(iObj);
    delete iObj;
}

void InputManager::processInputs()
{
    for (std::list<InputObject*>::iterator i= _objectList.begin()
            ;i != _objectList.end(); i++){
            if((*i)->isPressed())
                (*i)->callBack();
    }
}

void InputManager::processMessages()
{
    MessageStack *msgStack = MessageStack::getInstance();
    BaseMessage *msg = msgStack->popMessage(InputComponent);
    InputStack *inputStck = InputStack::getInstance();
    inputStck->getMouseXY(_mouseX, _mouseY);
    while (msg != nullptr) {
        switch(msg->getMessageType()) {
        case InputRegistration : {
            GenericMessage *gMsg = (GenericMessage*)msg;
            if (gMsg->findAttribute("InputObjectDelete") != -1) {
                GenericMsgAttribute *gAttr = gMsg->getAttribute("Token");
                int token = *(int*)gAttr->getAttribute();
            }
            else {
                GenericMsgAttribute *gAttrLst = gMsg->getAttribute("KeyList");
                GenericMsgAttribute *gAttrLstSz = gMsg->getAttribute("KeyListSize");
                GenericMsgAttribute *gAttrFunct = gMsg->getAttribute("Function");
                GenericMsgAttribute *gAttrParam = gMsg->getAttribute("Param");
                GenericMsgAttribute *gAttrToken = gMsg->getAttribute("Token");
                GenericMsgAttribute *gAttrIsMouseBtn = gMsg->getAttribute("MouseButton");
                addObject((int*)gAttrLst->getAttribute()
                          ,*(int*)gAttrLstSz->getAttribute()
                          ,*(int*)gAttrIsMouseBtn->getAttribute()
                          ,*(input_function*)gAttrFunct->getAttribute()
                          ,(void*)gAttrParam->getAttribute()
                          ,(int*)gAttrToken->getAttribute());
            }
            delete gMsg;
            break;
            }
        default :
            delete msg;
        }
        msg = msgStack->popMessage(InputComponent);
    }

    processInputs();
}

}
}
