#include "SceneManager.h"

namespace protosprite
{
namespace management
{
int SceneManager::_objectId = 0;
int SceneManager::_scenes = 0;

SceneManager::SceneManager()
{
    _sceneId = _scenes;
    _scenes++;
}

GameObject *SceneManager::createObject()
{
    GameObject *object = new GameObject(_objectId, _sceneId);
    _objectMap[_objectId] = object;
    _objectId++;
    return object;
}

GameObject* SceneManager::getObject(int objId)
{
    return _objectMap[objId];
}

void SceneManager::deleteObject(GameObject* obj)
{
    int objId = obj->getId();
    delete obj;
    _objectMap.erase(objId);
}

void SceneManager::makeScene()
{
}

void SceneManager::deleteScene()
{
    for(std::map<int,GameObject*>::iterator i = _objectMap.begin()
            ;i != _objectMap.end(); i++)
    {
        delete i->second;
    }
}

BaseAttribute *SceneManager::getAttribute(int objId, std::string name)
{
    GameObject *obj = _objectMap[objId];
    return obj->getAttribute(name);
}

void SceneManager::processMessages()
{
    MessageStack *msgStack = MessageStack::getInstance();
    BaseMessage *msg = msgStack->popMessage(SceneComponent);
    while (msg != nullptr) {
        switch(msg->getMessageType()) {
        case Attribute : {
            GenericMessage *gMsg = (GenericMessage*)msg;
            if (gMsg->findAttribute("RequestName") != -1) {
                GenericMsgAttribute *gAttrRequest = gMsg->getAttribute("RequestName");
                BaseAttribute *attr = getAttribute(gMsg->getObjectId()
                                                  ,gAttrRequest->getStringAttribute());
                if(attr)
                    msgStack->sendAttribute(gMsg->getSource()
                                           ,SceneComponent
                                           ,gMsg->getObjectId()
                                           ,gMsg->getSceneId()
                                           ,gAttrRequest->getStringAttribute()
                                           ,attr
                                           ,0);
            } else {
                GenericMsgAttribute *attrName = gMsg->getAttribute("Name");
                std::string name = attrName->getStringAttribute();

                if(!name.compare("position")) {
                    GenericMsgAttribute *gAttr = gMsg->getAttribute("Value");
                    GenericMsgAttribute *gAttrAdjustment = gMsg->getAttribute("Adjust");
                    Vec3Attribute *attribUpd = (Vec3Attribute*)(gAttr->getAttribute());
                    Vec3Attribute *attrib = (Vec3Attribute*)(_objectMap[gMsg->getObjectId()]->getAttribute("position"));
                    if (gAttrAdjustment->getIntAttribute()) {
                        math::Vec3 upd = attribUpd->getAttribute();
                        math::Vec3 orig = attrib->getAttribute();
                        attrib->setAttribute(orig + upd);
                        //DBG("changing: " << upd._x << " " << upd._y);
                    } else {
                        math::Vec3 orig = attrib->getAttribute();
                        math::Vec3 upd = attribUpd->getAttribute();
                        orig._x = upd._x;
                        orig._y = upd._y;
                        attrib->setAttribute(orig);
                        //DBG("updating: " << orig._x << " " << orig._y);
                    }
                    delete attribUpd;
                }

            }
            delete gMsg;
            break;
        }
        default :
            delete msg;
        }
        msg = msgStack->popMessage(SceneComponent);
    }
}




}
}
