#pragma once

#include <iostream>
#include <map>

#include "Attribute.h"
#include "MessageQueue.h"
#include "GameObject.h"


namespace protosprite
{
namespace management
{
class SceneManager
{
    static int _scenes;
    static int _objectId;

    int _sceneId;
    std::map<int,GameObject*> _objectMap;
public:
    SceneManager();
    int getSceneId() {return _sceneId;};
    GameObject *createObject();
    GameObject *getObject(int objId);
    BaseAttribute *getAttribute(int objId, std::string name);
    void deleteObject(GameObject *obj);
    void makeScene();
    void deleteScene();
    void sendAttribute(int objId, int sceneId, componentType dest
                            ,std::string name, BaseAttribute *value);
    void processMessages();
};
}
}
