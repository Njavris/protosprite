#include "Attribute.h"


namespace protosprite
{
namespace management
{
GenericMsgAttribute::GenericMsgAttribute(attributeType type, std::string attributeName, attributePtr *pAttr)
    : _type(type), _attributeName(attributeName)
{
    switch (type) {
        case attInt:
            _attr = new int(*(int*)pAttr);
            break;
        case attFloat:
            _attr = new float(*(float*)pAttr);
            break;
        case attVec2:
            _attr = new math::Vec2(((math::Vec2*)pAttr)->_x
                                  ,((math::Vec2*)pAttr)->_y);
            break;
        case attVec3:
            _attr = new math::Vec3(((math::Vec3*)pAttr)->_x
                                  ,((math::Vec3*)pAttr)->_y
                                  ,((math::Vec3*)pAttr)->_z);
            break;
        case attVec4:
            _attr = new math::Vec4(((math::Vec4*)pAttr)->_x
                                  ,((math::Vec4*)pAttr)->_y
                                  ,((math::Vec4*)pAttr)->_z
                                  ,((math::Vec4*)pAttr)->_w);
            break;
        case attString:
            _attr = new std::string(*(std::string*)pAttr);
            break;
        case attPtr:
            _attr = pAttr;
            break;
        case attMat4:
        default:
            break;
    }
}

GenericMsgAttribute::~GenericMsgAttribute()
{
    switch (_type) {
        case attInt:
            delete (int*)_attr;
            break;
        case attFloat:
            delete (float*)_attr;
            break;
        case attVec2:
            delete (math::Vec2*)_attr;
            break;
        case attVec3:
            delete (math::Vec3*)_attr;
            break;
        case attVec4:
            delete (math::Vec4*)_attr;
            break;
        case attString:
            delete (std::string*)_attr;
            break;
        case attPtr:
        case attMat4:
        default:
            break;
    }
}
}
}
