#include "MessageQueue.h"

namespace protosprite
{
namespace management
{
MessageStack *MessageStack::_instance = nullptr;

MessageStack *MessageStack::getInstance()
{
    if (!_instance) {
        _instance = new MessageStack();
        _instance->initQueues();
    }
    return _instance;
}

void MessageStack::initQueues()
{
    for (int i = AllComponents; i != DummyComponent; i++) {
        _queues.push_back((std::queue<BaseMessage*>*)nullptr);
    }
}

void MessageStack::clearQueue(int queueNr)
{
    delete _queues[queueNr];
}

BaseMessage *MessageStack::popMessage(int queueNr)
{
    if (!_queues[queueNr])
        return nullptr;
    if (_queues[queueNr]->empty())
        return nullptr;
    BaseMessage *ret = _queues[queueNr]->front();
    _queues[queueNr]->pop();
    return ret;
}

void MessageStack::pushMessage(BaseMessage *msg, int queueNr)
{
    if (!_queues[queueNr])
        _queues[queueNr] = new std::queue<BaseMessage*>();
    _queues[queueNr]->push(msg);
}

void MessageStack::requestAttribute(componentType dst, componentType src, int objId, int sceneId, std::string name)
{
    GenericMessage *msg = new GenericMessage(dst, Attribute, src, sceneId, objId);
    msg->addStringAttribute("RequestName", name);
    pushMessage((BaseMessage*)msg,dst);
}

void MessageStack::sendAttribute(componentType dst, componentType src, int objId
                       ,int sceneId, std::string name, BaseAttribute* attrib, int change)
{
    GenericMessage *msg = new GenericMessage(dst, Attribute, src, sceneId, objId);
    msg->addStringAttribute("Name", name);
    msg->addAttribute(attPtr, "Value", (attributePtr*)attrib);
    msg->addIntAttribute("Adjust", change);
    pushMessage((BaseMessage*)msg,dst);
}
}
}
