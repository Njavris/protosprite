#pragma once

#include <iostream>
#define DBG(msg)    std::cout << __func__ << \
                    "():" << msg << std::endl
/* for callback */
typedef void (*input_function)(void*);
typedef void attributePtr;

/* Message types */
typedef enum {
    Normal,
    Registration,
    Attribute,
    Event,
    InputRegistration,
} msgType;

/* components */
typedef enum {
    AllComponents,
    SceneComponent,
    GraphicsComponent,
    InputComponent,
    ScriptingCompoent,
    ObjectTreeComponent,
    DummyComponent,
} componentType;

/* Attributes */
typedef enum {
    attInt,
    attFloat,
    attVec2,
    attVec3,
    attVec4,
    attMat4,
    attString,
    attPtr,
    attNone,
} attributeType;
