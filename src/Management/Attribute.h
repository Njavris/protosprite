#pragma once

#include <string>
#include <iostream>

#include "../Math/Math.h"
#include "Types.h"

namespace protosprite
{
namespace management
{
class BaseAttribute
{
public:
    virtual ~BaseAttribute() = 0;
};
inline BaseAttribute::~BaseAttribute() {};

class GenericMsgAttribute
{
    attributeType _type;
    attributePtr *_attr;
    std::string _attributeName;
public:
    GenericMsgAttribute(attributeType type, std::string attributeName, attributePtr *pAttr);
    ~GenericMsgAttribute();
    const attributeType getType() {return _type;};
    attributePtr* getAttribute() {return _attr;};
    const int getIntAttribute()
    {
        int ret = *(int*)_attr;
        return ret;
    };
    const float getFloatAttribute()
    {
        float ret = *(float*)_attr;
        return ret;
    };
    const std::string getStringAttribute()
    {
        std::string ret = *(std::string*)_attr;
        return ret;
    }
    std::string getAttrName() {return _attributeName;};
};

class IntegerAttribute : public BaseAttribute
{
    int _attributeVal;
public:
    IntegerAttribute(int attrib) : _attributeVal(attrib) {};
    void setAttribute(int attrib) {_attributeVal = attrib;};
    const int getAttribute() const {return _attributeVal;};
};

class Vec3Attribute : public BaseAttribute
{
    math::Vec3 _attributeVal;
public:
    Vec3Attribute(math::Vec3 attrib) : _attributeVal(attrib) {};
    Vec3Attribute(float x, float y, float z) {_attributeVal = math::Vec3(x,y,z);};
    void setAttribute(math::Vec3 attrib) {_attributeVal = attrib;};
    math::Vec3 getAttribute() const {return _attributeVal;};
};

}
}
