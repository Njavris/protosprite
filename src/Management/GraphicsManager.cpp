#include "GraphicsManager.h"

namespace protosprite
{
namespace management
{
GraphicsManager *GraphicsManager::_instance = nullptr;

GraphicsManager* GraphicsManager::getInstance()
{
    if(_instance == nullptr) _instance = new GraphicsManager();
    return _instance;
}

GraphicsObject *GraphicsManager::getGraphicsObject(int sceneId, int objectId)
{
    for (std::list<GraphicsObject*>::iterator i= _objectList.begin()
            ;i != _objectList.end(); i++){
        if ((*i)->getSceneId() == sceneId && (*i)->getId() == objectId) {
            return *i;
        }
    }
    return nullptr;
}

void GraphicsManager::addObject(int sceneId, int objectId)
{
    GraphicsObject *gObj = new GraphicsObject(sceneId, objectId);
    _objectList.push_back(gObj);
}

void GraphicsManager::deleteObject(int sceneId, int objectId)
{
    GraphicsObject *gObj = getGraphicsObject(sceneId, objectId);
    if (gObj)
        _objectList.remove(gObj);
}

void GraphicsManager::processMessages()
{
    MessageStack *msgStack = MessageStack::getInstance();
    BaseMessage *msg = msgStack->popMessage(GraphicsComponent);
    while (msg != nullptr) {
        switch(msg->getMessageType()) {
        case Attribute : {
            GenericMessage *gMsg = (GenericMessage*)msg;
            GenericMsgAttribute *gAttrName = gMsg->getAttribute("Name");
            GenericMsgAttribute *gAttrValue = gMsg->getAttribute("Value");
            updateAttribute(gMsg->getObjectId()
                            ,gMsg->getSceneId()
                            ,gAttrName->getStringAttribute()
                            ,(BaseAttribute*)gAttrValue->getAttribute());
            delete gMsg;
            break;
        }
        case Registration : {
            GenericMessage *gMsg = (GenericMessage*)msg;
            if (gMsg->findAttribute("Delete") != -1)
                deleteObject(gMsg->getSceneId(), gMsg->getObjectId());
            else {
                addObject(gMsg->getSceneId(), gMsg->getObjectId());
                msgStack->requestAttribute(SceneComponent
                                           ,GraphicsComponent
                                           ,gMsg->getObjectId()
                                           ,gMsg->getSceneId()
                                           ,"position");
            }
            delete gMsg;
            break;
        }
        default :
            delete msg;
        }
        msg = msgStack->popMessage(GraphicsComponent);
    }
    /* For next execution */
    poll();
}

void GraphicsManager::updateAttribute(int objectId, int sceneId, std::string attributeName, BaseAttribute *value)
{
    GraphicsObject *gObj;
    for (std::list<GraphicsObject*>::iterator i = _objectList.begin();
            i != _objectList.end(); i++) {
        if ((((GraphicsObject*)(*i))->getSceneId() == sceneId)
                &&(((GraphicsObject*)(*i))->getId() == objectId))
                    gObj = *i;
    }
    GraphicsCalls *grphcs = GraphicsCalls::getInstance();
    TextGroup *txtgrp = gObj->getTextGroup();
    if (!attributeName.compare("position")) {
        math::Vec3 position = ((Vec3Attribute*)value)->getAttribute();
        if (gObj->getLayerId() != 0) {
            //grphcs->moveLayer(gObj->getLayerId(), position._x, position._y, position._z);
            grphcs->setLayerPosition(gObj->getLayerId(), position._x, position._y, position._z);
        }
        else if (txtgrp != nullptr) {
            txtgrp->setGroupPosition(position._x,position._y,position._z);
        }
    }
}

/* Should be used sparringly */
void GraphicsManager::poll()
{
    MessageStack *msgStack = MessageStack::getInstance();
    for (std::list<GraphicsObject*>::iterator i= _objectList.begin()
            ;i != _objectList.end(); i++){
        msgStack->requestAttribute(SceneComponent
                                ,GraphicsComponent
                                ,(*i)->getId()
                                ,(*i)->getSceneId()
                                ,"position");
    }
}
}
}
