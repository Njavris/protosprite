#include "GameObject.h"

namespace protosprite
{
namespace management
{
GameObject::GameObject(int objectId, int sceneId)
    :_objectId(objectId), _sceneId(sceneId), _lock(0)
{
}

GameObject::~GameObject()
{
    for (std::map<std::string, BaseAttribute*>::iterator i = _attributes.begin();
            i != _attributes.end(); i++)
        delete i->second;
}

void GameObject::addComponent(componentType comp)
{
    for (std::list<componentType>::iterator i = _components.begin();
                        i != _components.end(); i++)
        if (*i == comp)
            return;
    _components.push_back(comp);
    GenericMessage *msg = new GenericMessage(comp, Registration, ObjectTreeComponent, getSceneId(), getId());
    MessageStack *msgStack = MessageStack::getInstance();
    msgStack->pushMessage((BaseMessage*)msg,comp);
}

void GameObject::removeComponent(componentType comp)
{
    for (std::list<componentType>::iterator i = _components.begin();
                        i != _components.end(); i++)
        if (*i == comp)
            _components.erase(i);
}

void GameObject::addAttribute(std::string name, BaseAttribute *value)
{
    _attributes[name] = value;
}

void GameObject::removeAttribute(std::string name)
{
    delete _attributes[name];
    _attributes.erase(name);
}

BaseAttribute *GameObject::getAttribute(std::string name)
{
    return _attributes[name];
}

void GameObject::lock()
{
    while (__sync_lock_test_and_set(&_lock, 1))
        Sleep(10);
}

void GameObject::unlock()
{
    __sync_lock_release(&_lock, 0);
}

}
}
