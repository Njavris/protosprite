#pragma once
#include "../Graphics/GraphicsCalls.h"

namespace protosprite
{
namespace game
{
class Player
{
    math::Vec3 _position;
    math::Vec2 _direction;
    math::Vec2 _velocity;
    int _layerId;
public:
    Player(math::Vec3 pos, math::Vec2 dir, math::Vec2 veloc, int layerId);
    ~Player();
    void Shoot();
    void Move(math::Vec2 dir) {setDir(dir);};
    void Update(double deltaTime);

    const math::Vec3 getPos() const {return _position;};
    const math::Vec2 getDir() const {return _direction;};
    const math::Vec2 getVel() const {return _velocity;};

    void setPos(math::Vec3 pos) {_position = pos;};
    void setDir(math::Vec2 dir) {_direction = dir;};
    void setVel(math::Vec2 vel) {_velocity = vel;};
    void setVelX(double xVel) {_velocity._x = xVel;};
    void setVelY(double yVel) {_velocity._y = yVel;};
};
}
}
