#include "Player.h"

namespace protosprite
{
namespace game
{
Player::Player(math::Vec3 pos, math::Vec2 dir, math::Vec2 veloc, int layerId)
    :_position(pos),_direction(dir), _velocity(veloc), _layerId(layerId)
{

}

Player::~Player()
{
}
//spawn bullet instances from playrs  ars
void Player::Shoot()
{

}

void Player::Update(double deltaTime)
{
    graphics::GraphicsCalls* graphics = graphics::GraphicsCalls::getInstance();
    _position._x += _velocity._x * deltaTime;
    _position._y += _velocity._y * deltaTime;
    graphics->setLayerPosition(_layerId, _position._x, _position._y, _position._z);
}
}
}
