//main to test functionality
#include <iostream>

#include "level.h"
#include "../Utils/Timer.h"
#include "../Graphics/GraphicsCalls.h"
#include "../Input/InputStack.h"
#include "../Management/SceneManager.h"
#include "../Management/GraphicsManager.h"
#include "../Management/MessageQueue.h"
#include "../Management/InputManager.h"
#include "Player.h"

using namespace protosprite::graphics;
using namespace protosprite::input;
using namespace protosprite::utils;
using namespace protosprite::math;
using namespace protosprite::game;
using namespace protosprite::management;

float speed = 0.1f;

void moveUp(void *param)
{
    GameObject *obj = (GameObject*)param;
    MessageStack *msgStack = MessageStack::getInstance();
    msgStack->sendAttribute(SceneComponent
                            ,DummyComponent
                            ,obj->getId()
                            ,obj->getSceneId()
                            ,"position"
                            ,new Vec3Attribute(0,speed,0)
                            ,1);
}

void moveDown(void *param)
{
    GameObject *obj = (GameObject*)param;
    MessageStack *msgStack = MessageStack::getInstance();
    msgStack->sendAttribute(SceneComponent
                            ,DummyComponent
                            ,obj->getId()
                            ,obj->getSceneId()
                            ,"position"
                            ,new Vec3Attribute(0,-speed,0)
                            ,1);
}

void moveLeft(void *param)
{
    GameObject *obj = (GameObject*)param;
    MessageStack *msgStack = MessageStack::getInstance();
    msgStack->sendAttribute(SceneComponent
                            ,DummyComponent
                            ,obj->getId()
                            ,obj->getSceneId()
                            ,"position"
                            ,new Vec3Attribute(-speed,0,0)
                            ,1);
}

void moveRight(void *param)
{
    GameObject *obj = (GameObject*)param;
    MessageStack *msgStack = MessageStack::getInstance();
    msgStack->sendAttribute(SceneComponent
                            ,DummyComponent
                            ,obj->getId()
                            ,obj->getSceneId()
                            ,"position"
                            ,new Vec3Attribute(speed,0,0)
                            ,1);
}

int main()
{
    int gameIsRunning = 0;
    int textureSize = 32;
    int letterSize = 16;
    int screenWidth = 28;
    int screenHeight = 18;
    int screenDepth = 2;
    int screenXReso = 896;
    int screenYReso = 576;
    int atlasWidth = 32;
    int texArraySize = 8;
    int levelWidth = lvlSize[0];
    int levelHeight = lvlSize[1];
    int testAtlasId, textAtlasId, imageAtlasId;

    GraphicsCalls* graphicsCalls = new GraphicsCalls(textureSize,
                                                        textureSize,
                                                        texArraySize,
                                                        screenWidth,
                                                        screenHeight,
                                                        screenDepth,
                                                        screenXReso,
                                                        screenYReso,
                                                        atlasWidth,
                                                        atlasWidth);
    Window* window = graphicsCalls->getWindow();
    InputStack* inputStack = InputStack::getInstance();

    InputManager *inputMngr = InputManager::getInstance();
    inputMngr->setScale(screenWidth + 4, 2*screenHeight - 4);
    inputMngr->setResolution(screenXReso, screenYReso);
    SceneManager *sceneMngr = new SceneManager();
    GraphicsManager *graphicsMngr = GraphicsManager::getInstance();
    MessageStack *msgStack = MessageStack::getInstance();

    Drawer* drawMngr = graphicsCalls->getDrawer();

    const char* ImageFileNames[] = {"src\\dngn_lava.png",
                                    "src\\hippogriff.png",
                                    "src\\angel.png",
                                    "src\\centaur_warrior.png",
                                    "src\\cyclops.png",
                                    "src\\dragon.png",
                                    "src\\norris_with_board.png"};
    int *imageIds;

    GameObject *playerObj = sceneMngr->createObject();
    playerObj->addComponent(GraphicsComponent);
    playerObj->addComponent(InputComponent);
    playerObj->addAttribute("position", new Vec3Attribute(Vec3(5,10,0)));

    graphicsMngr->processMessages();
    GraphicsObject *playerGObj = graphicsMngr->getGraphicsObject(playerObj->getSceneId(), playerObj->getId());


    char* fpsStr = (char*)calloc(1,5*sizeof(char));
    int inptIncr = 0;
    double mouseX, mouseY;
    Timer *FPStimer = new Timer();
    Timer *InputTimer = new Timer();

    imageAtlasId = graphicsCalls->fillAtlas(atlasWidth, textureSize, 7, ImageFileNames, &imageIds);
    testAtlasId = graphicsCalls->addAtlas("src\\testset.png",23, 21, textureSize, textureSize);
    textAtlasId = graphicsCalls->addAtlas("src\\font.png", 16, 6, letterSize, letterSize);
    graphicsCalls->commitTextures();

    int bckgrndLyr1 = graphicsCalls->addLayer(Vec2(1,1), Vec3(0,0,0));
    int bckgrndLyr2 = graphicsCalls->generateLayer(Vec3(0,0,0), lvlSize, lvlTiles, testAtlasId);
    int playerLyr = graphicsCalls->addLayer(Vec2(1,1), Vec3(0,0,0));
    int bckgrndLyr = graphicsCalls->addLayer(Vec2(1,1), Vec3(0,0,0));

    playerGObj->setLayerId(playerLyr);

    graphicsCalls->addTileToLayer(playerLyr, Vec2(1,1), Vec3(0,0,0), imageAtlasId, 7);

    TextGroup* fps = new TextGroup(fpsStr,strlen(fpsStr),drawMngr->_texId[textAtlasId]);
    fps->setSize(0.5f,0.5f);
    fps->setGroupPosition(0,screenHeight - 0.5,0);
    graphicsCalls->bindGroupToLayer(bckgrndLyr, (TileGroup*)fps);

    int keyLists[][2] = {{GLFW_KEY_W, ~GLFW_KEY_S} //UP
                      ,{GLFW_KEY_S, ~GLFW_KEY_W} //DOWN
                      ,{GLFW_KEY_A, ~GLFW_KEY_D} //LEFT
                      ,{GLFW_KEY_D, ~GLFW_KEY_A}}; //RIGHT
    input_function functions[4] = {moveUp
                                  ,moveDown
                                  ,moveLeft
                                  ,moveRight};
    int tokens[5];
    for (int i = 0; i < 4; i++) {
        GenericMessage *msg = new GenericMessage(InputComponent, InputRegistration, ObjectTreeComponent, 0, 0);
        msg->addIntAttribute("KeyListSize", 2);
        msg->addAttribute(attPtr, "KeyList", keyLists[i]);
        msg->addAttribute(attPtr, "Function", (attributePtr*)&functions[i]);
        msg->addAttribute(attPtr, "Param", (void*)playerObj);
        msg->addAttribute(attPtr, "Token", &(tokens[i]));
        msg->addIntAttribute("MouseButton", 0);
        msgStack->pushMessage((BaseMessage*)msg, InputComponent);
    }
    GenericMessage *msg = new GenericMessage(InputComponent, InputRegistration, ObjectTreeComponent, 0, 0);
    int mb = 1;
    msg->addIntAttribute("KeyListSize", 1);
    msg->addAttribute(attPtr, "KeyList", &mb);
    msg->addAttribute(attPtr, "Function", (attributePtr*)&functions[0]);
    msg->addAttribute(attPtr, "Param", (void*)playerObj);
    msg->addAttribute(attPtr, "Token", &(tokens[4]));
    msg->addIntAttribute("MouseButton", 1);
    msgStack->pushMessage((BaseMessage*)msg, InputComponent);

    graphicsCalls->setUniforms();
    while(window->isOpen())
    {
        graphicsCalls->drawFrame();
        FPStimer->incFrame();
        if(FPStimer->getTime() >= 0.1d)
        {
            sprintf(fpsStr,"%d",FPStimer->getFPS());
            fps->setString(fpsStr,strlen(fpsStr));
            FPStimer->reset();
        }

        if(InputTimer->getTime() >= 0.01d)
        {
            graphicsMngr->processMessages();
            sceneMngr->processMessages();
            inputMngr->processMessages();
            inputMngr->processInputs();
            /*inputMngr->getMouseXY(mouseX, mouseY);
            //std::cout << mouseX << " " << mouseY << std::endl;
            MessageStack *msgStack = MessageStack::getInstance();
            msgStack->sendAttribute(SceneComponent
                                    ,DummyComponent
                                    ,playerObj->getId()
                                    ,playerObj->getSceneId()
                                    ,"position"
                                    ,new Vec3Attribute(mouseX,mouseY,0)
                                    ,0);*/

            if((inputStack->isKbPressed(GLFW_KEY_Z) || inputStack->isKbPressed(GLFW_KEY_SPACE))
               && inptIncr >=40)
            {
                if(inputStack->isKbPressed(GLFW_KEY_SPACE))
                {
                    if(fps->isBound())
                        graphicsCalls->unbindGroupFromLayer((TileGroup*)fps);
                    else if(!fps->isBound())
                        graphicsCalls->bindGroupToLayer(bckgrndLyr, (TileGroup*)fps);
                }
                if(inputStack->isKbPressed(GLFW_KEY_Z))
                {
                    if(graphicsCalls->isMeshMode())
                        graphicsCalls->setFillMode();
                    else if(graphicsCalls->isFillMode())
                        graphicsCalls->setMeshMode();
                }
                inptIncr = 0;
            }
            inptIncr ++;

            InputTimer -> reset();
        }
        if(inputStack->isKbPressed(GLFW_KEY_ESCAPE)) window->Close();
    }
    sceneMngr->deleteScene();
    return 0;
}
