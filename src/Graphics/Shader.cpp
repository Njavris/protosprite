#include "Shader.h"

namespace protosprite
{
namespace graphics
{
    Shader::Shader(const char *vertPath, const char *fragPath) :_vertPath(vertPath), _fragPath(fragPath)
    {
        _logger = utils::DebugLogger::getInstance();
        shaderID = load();
    }

    Shader::~Shader()
    {
        glDeleteProgram(shaderID);
    }

    GLuint Shader::load()
    {
        char* error;
        GLuint program = glCreateProgram();
        GLuint vertex = glCreateShader(GL_VERTEX_SHADER);
        GLuint fragment = glCreateShader(GL_FRAGMENT_SHADER);

        const char* vertSource = utils::FileUtils::readFile(_vertPath);
        const char* fragSource = utils::FileUtils::readFile(_fragPath);

        glShaderSource(vertex, 1, &vertSource, NULL);
        glCompileShader(vertex);
        GLint result;
        glGetShaderiv(vertex, GL_COMPILE_STATUS, &result);
        if (result == GL_FALSE)
        {
            GLint length;
            glGetShaderiv(vertex, GL_INFO_LOG_LENGTH, &length);
            error = (char*)malloc(length);
            glGetShaderInfoLog(vertex, length, &length, error);
            _logger->logMessage("Graphics::Shader: Vertex Shader", error,1);
            glDeleteShader(vertex);
            return 0;
        }

        glShaderSource(fragment, 1, &fragSource, NULL);
        glCompileShader(fragment);
        glGetShaderiv(fragment, GL_COMPILE_STATUS, &result);
        if (result == GL_FALSE)
        {
            GLint length;
            glGetShaderiv(fragment, GL_INFO_LOG_LENGTH, &length);
            error = (char*)malloc(length);
            glGetShaderInfoLog(fragment, length, &length, error);
            _logger->logMessage("Graphics::Shader: Fragment Shader", error,1);
            glDeleteShader(fragment);
            return 0;
        }

        glAttachShader(program, vertex);
        glAttachShader(program, fragment);

        glLinkProgram(program);
        glValidateProgram(program);

        glDeleteShader(vertex);
        glDeleteShader(fragment);

        return program;
    }
    void Shader::enable() const
    {
        glUseProgram(shaderID);
    }

    void Shader::disable() const
    {
        glUseProgram(0);
    }

    GLint Shader::getUniformLocation(const GLchar *name)
    {
        return glGetUniformLocation(shaderID, name);
    }

    void Shader::setUniform1i(const GLchar *name, int integer)
    {
        glUniform1i(getUniformLocation(name), integer);
    }

    void Shader::setUniform1iv(const GLchar *name, int *integer, int count)
    {
        glUniform1iv(getUniformLocation(name), count, integer);
    }

    void Shader::setUniform1uiv(const GLchar *name, unsigned int *integer, int count)
    {
        glUniform1uiv(getUniformLocation(name), count, integer);
    }

    void Shader::setUniform1f(const GLchar *name, float number)
    {
        glUniform1f(getUniformLocation(name), number);
    }

    void Shader::setUniform1fv(const GLchar *name, float *number, int count)
    {
        glUniform1fv(getUniformLocation(name), count, number);
    }

    void Shader::setUniformVec2(const GLchar *name, const math::Vec2 &vector)
    {
        glUniform2f(getUniformLocation(name), vector._x, vector._y);
    }
    void Shader::setUniformVec3(const GLchar *name, const math::Vec3 &vector)
    {
        glUniform3f(getUniformLocation(name), vector._x, vector._y, vector._z);
    }

    void Shader::setUniformVec4(const GLchar *name, const math::Vec4 &vector)
    {
        glUniform4f(getUniformLocation(name), vector._x, vector._y, vector._z, vector._w);
    }

    void Shader::setUniformMat4(const GLchar *name, const math::Mat4 &matrix)
    {
        glUniformMatrix4fv(getUniformLocation(name), 1, GL_FALSE, matrix._elements);
    }

    void Shader::setUniform2fv(const GLchar *name, GLfloat *value, int size)
    {
        glUniform2fv(getUniformLocation(name), size, value);
    }
}
}
