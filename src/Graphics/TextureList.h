#pragma once

#include <map>

#include "Texture.h"

namespace protosprite
{
namespace graphics
{
class TextureList
{
    std::map<GLuint,Texture*> *_textures;
    std::map<GLuint,Texture*>::iterator _texIter;
public:
    TextureList();
    ~TextureList();
    const Texture *getTexture(GLuint textureNr) const;
    const GLuint addTexture(const char* filename);
    const GLuint addTexture(const unsigned char* image, int width, int height, int size);
    void unloadAll();
    void loadAll();
    const unsigned int getSize();
    GLuint getFirst();
    GLuint getNext();
};
}
}
