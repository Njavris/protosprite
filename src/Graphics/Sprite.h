#pragma once

#include "Drawable.h"

//mostly useless, good only for vectorless overloaded constructors
namespace protosprite
{
namespace graphics
{
class Sprite : public Drawable
{
public:
    Sprite(float x, float y, float width, float height, const math::Vec4& colour)
        : Drawable(math::Vec2(width,height), math::Vec3(x,y,0), colour)
    {
    }
    Sprite(float x, float y, float width, float height, GLuint texID)
        : Drawable(math::Vec2(width,height), math::Vec3(x,y,0),texId)
    {
    }
};
}
}
