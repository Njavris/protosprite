#pragma once

#include <map>

#include "Layer.h"

namespace protosprite
{
namespace graphics{
class LayerList
{
    std::map<int,Layer*> *_layers;
    std::map<int,Layer*>::iterator _layerIter;
    int _size;
public:
    LayerList();
    ~LayerList();
    Layer *getLayer(int lyerId);
    const int addLayer(Layer *layer);
    const int addLayer();
    const unsigned int getSize();
    int getFirst();
    int getNext();
};
}
}
