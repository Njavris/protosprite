#include "Window.h"
namespace protosprite
{
namespace graphics
{
    void GLErrorCallback(int error, const char* description)
    {
        utils::DebugLogger *logger = utils::DebugLogger::getInstance();
        logger->logMessage("OpenGL Error: ",description,1);
    }

    void keyCallback(GLFWwindow * window, int key, int scancode, int action, int mods)
    {
        InputStack* inputStack = InputStack::getInstance();
        inputStack->_keys[key] = action != GLFW_RELEASE;
    }

    void mouseButtonCallback(GLFWwindow *window, int button, int action, int mods)
    {
        InputStack* inputStack = InputStack::getInstance();
        inputStack->_mouseButtons[button] = action != GLFW_RELEASE;
    }

    void mousePositionCallback(GLFWwindow *window, double xpos, double ypos)
    {
        InputStack* inputStack = InputStack::getInstance();
        inputStack->_mouseXY[0] = xpos;
        inputStack->_mouseXY[1] = ypos;
    }

    Window::Window(const char *title, int width, int height)
        :_title(title), _width(width), _height(height)
    {
        _logger = utils::DebugLogger::getInstance();

        if (Init())
        {
            glfwTerminate();
        }

        _logger->logMessage("OpenGL version",(const char*)glGetString(GL_VERSION),1);
    }

    Window::~Window()
    {
        glfwTerminate();
    }

    int Window::Init()
    {
        glewExperimental = true;
        if (!glfwInit())
        {
            _logger->logMessage("Graphics::Window","Failed to init GLFW",1);
            return 1;
        }
        glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

        _window = glfwCreateWindow(_width, _height, _title, NULL, NULL);
        if (!_window)
        {
            glfwTerminate();
            _logger->logMessage("Graphics::Window","Failed to create window!",1);
            return 1;
        }

        glfwMakeContextCurrent(_window);
        glfwSetKeyCallback(_window, keyCallback);
        glfwSetMouseButtonCallback(_window, mouseButtonCallback);
        glfwSetCursorPosCallback(_window, mousePositionCallback);
        glfwSetWindowUserPointer(_window, this);
        glfwSwapInterval(0);

        glfwSetErrorCallback(GLErrorCallback);
        glfwSetInputMode(_window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

        glewExperimental = GL_TRUE;
        if (glewInit() != GLEW_OK)
        {
            _logger->logMessage("Graphics::Window","Failed to init GLEW",1);
            return 1;
        }
        glViewport(0, 0, _width, _height);

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        return 0;
    }

    void Window::Clear() const
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    void Window::Update()
    {
        glfwPollEvents();
        glfwSwapBuffers(_window);
    }

    bool Window::isOpen() const
    {
        return !(glfwWindowShouldClose(_window) == 1);
    }

    void Window::setWidthHeight(int width, int height)
    {
        _width = width;
        _height = height;
        glfwSetWindowSize(_window, _width, _height);
    };

    void Window::Close()
    {
        _logger->logMessage("Graphics::Window","Closing window",1);
        glfwSetWindowShouldClose(_window, GL_TRUE);
    }

    const int Window::getWidth()const
    {
        return _width;
    };
    const int Window::getHeight()const
    {
        return _height;
    };
}
}
