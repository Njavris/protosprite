#include "LayerList.h"

namespace protosprite
{
namespace graphics{
    LayerList::LayerList()
    {
        _layers = new std::map<int,Layer*>();
        _size = 0;
    };

    LayerList::~LayerList()
    {
        delete(_layers);
    };

    Layer* LayerList::getLayer(int lyerId)
    {
        if(!_size)return nullptr;
        try
        {
            return _layers->at(lyerId);
        }
        catch(const std::out_of_range &outOfRange)
        {
            return nullptr;
        }
    }

    const int LayerList::addLayer(Layer *layer)
    {

        _layers->insert(std::pair<int,Layer*>(_size+1,layer));
        _size++;
        return _size;
    }

    const int LayerList::addLayer()
    {
    }

    const unsigned int LayerList::getSize()
    {
        return _layers->size();
    }

    int LayerList::getFirst()
    {
        if(!_size)return -1;
        _layerIter = _layers->begin();
        return (_layerIter->first);
    }

    int LayerList::getNext()
    {
        if(_layerIter == _layers->end())return -1;
        _layerIter++;
        return _layerIter->first;
    }
}
}
