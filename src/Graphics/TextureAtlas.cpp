#include "TextureAtlas.h"

namespace protosprite
{
namespace graphics
{
    TextureAtlas::TextureAtlas(int atlasWidth, int texWidth)
        :_atlasWidth(atlasWidth),_atlasHeight(atlasWidth),_texWidth(texWidth),_texHeight(texWidth),_lastPos(0)
    {
        _image = (unsigned char*)calloc(1,(atlasWidth * texWidth * 4) * (atlasWidth * texWidth * 4) * sizeof(unsigned char));
    }

    TextureAtlas::TextureAtlas(int atlasWidth, int atlasHeight, int texWidth, int texHeight)
        :_atlasWidth(atlasWidth),_atlasHeight(atlasHeight),_texWidth(texWidth),_texHeight(texHeight),_lastPos(0)
    {
        _image = (unsigned char*)calloc(1,(atlasWidth * texWidth * 4) * (atlasHeight * texHeight * 4) * sizeof(unsigned char));
    }

    TextureAtlas::~TextureAtlas()
    {
        free(_image);
    }

    const int TextureAtlas::addTexture(const unsigned char* texture)
    {
        _lastPos = addToAtlas(_image, _atlasWidth, _atlasHeight, _texWidth, _texHeight, _lastPos, texture);
        return _lastPos;
    }

    void TextureAtlas::setTexture(const unsigned char* texture)
    {
        memcpy(_image, texture ,getAtlasSize());
    }

    int TextureAtlas::addToAtlas(unsigned char* atlas, int atlasWidth, int atlasHeight, int width, int height, int lastpos, const unsigned char* image)
    {
        int position = lastpos + 1;
        int atlasSize = atlasWidth * atlasHeight;
        if(lastpos >= atlasSize) return -1;

        int xPos = (lastpos % atlasWidth) * width;
        int yPos = (lastpos / atlasWidth) * width;
        for(int y = 0; y < height; y++)
        {
            for(int x = 0; x < width; x++)
            {
                for(int byte = 0; byte < 4; byte++)
                {
                    int dest = ((yPos + y) * atlasWidth * width * 4) + ((xPos + x) * 4) + byte;
                    int src = (y * width * 4) + (x * 4) + byte;
                    atlas[dest] = image[src];
                }
            }
        }
        return position;
    }

    const int TextureAtlas::getAtlasWidth() const
    {
        return _atlasWidth;
    };
    const int TextureAtlas::getAtlasHeight() const
    {
        return _atlasHeight;
    };
    const int TextureAtlas::getAtlasWidthPx() const
    {
        return _atlasWidth * _texWidth;
    };
    const int TextureAtlas::getAtlasHeightPx() const
    {
        return _atlasHeight * _texHeight;
    };
    const int TextureAtlas::getAtlasSize() const
    {
        return _atlasHeight * _atlasWidth * _texWidth * _texHeight * 4;
    };
    const int TextureAtlas::getTextureWidth() const
    {
        return _texWidth;
    };
    const int TextureAtlas::getLastPos() const
    {
        return _lastPos;
    };
    const unsigned char* TextureAtlas::getAtlas() const
    {
        return _image;
    };
}
}
