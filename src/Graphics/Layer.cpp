#include "Layer.h"

namespace protosprite
{
namespace graphics
{
    Layer::Layer(math::Vec2 size, math::Vec3 position)
        : _size(size), _position(position)
    {
        _drawables = new DrawableList();
    };

    Layer::~Layer()
    {
        delete _drawables;
    };

    void Layer::setFirstDrawable()
    {
        _drawables->setFirstDrawable();
    };

    void Layer::setNextDrawable()
    {
        _drawables->setNextDrawable();//++;
    };

    Drawable* Layer::getDrawable()
    {
        return _drawables->getDrawable();
    };

    void Layer::addDrawable(Drawable* drawable)
    {
        _drawables->addDrawable(drawable);
    };

    void Layer::removeDrawable(Drawable* drawable)
    {
        _drawables->removeDrawable(drawable);
    };

    void Layer::addGroup(Layer* group)
    {
        group->setFirstDrawable();
        while(group->getDrawable() != nullptr)
        {
            if(!(_drawables->doesExist(group->getDrawable())))
               addDrawable(group->getDrawable());
            group->setNextDrawable();
        }
    };

    void Layer::setSize(float x, float y)
    {
        _size._x = x;
        _size._y = y;
    };

    math::Vec2& Layer::getSize()
    {
        return _size;
    };

    void Layer::multiplyLayerSize(math::Vec2 size)
    {
        _size.multiply(size);
    };

    void Layer::setLayerPosition(float x, float y, float z)
    {
        _position._x = x;
        _position._y = y;
        _position._z = z;
    };

    void Layer::changeLayerPosition(float x, float y, float z)
    {
        _position._x += x;
        _position._y += y;
        _position._z += z;
    };

    const math::Vec2& Layer::getLayerSize() const
    {
        return _size;
    };

    const math::Vec3& Layer::getLayerPosition() const
    {
        return _position;
    };
}
}
