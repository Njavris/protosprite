#pragma once

#include "../Utils/Defines.h"
#include "Drawable.h"

namespace protosprite
{
namespace graphics
{
class Letter: public Drawable
{
    char _letter;
public:
    Letter(math::Vec2 size, math::Vec3 position,GLuint texId, char letter);
    void setLetter(char letter);
    char getLetter();
};
}
}
