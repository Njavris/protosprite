#include "TileGroup.h"

namespace protosprite
{
namespace graphics
{
    TileGroup::TileGroup()
        :Layer(math::Vec2(1,1), math::Vec3(0,0,0)), _layer(nullptr), _bound(0)
    {

    };

    TileGroup::~TileGroup()
    {
        unbindFromLayer();
    };

    void TileGroup::setGroupPosition(float x, float y, float z)
    {
        const math::Vec3 positionOld = Layer::getLayerPosition();
        Drawable* drawable;
        Layer::setFirstDrawable();
        while(Layer::getDrawable() != nullptr)
        {
            drawable = Layer::getDrawable();
            drawable->changePosition(math::Vec3(-positionOld._x,-positionOld._y,-positionOld._z));
            drawable->changePosition(math::Vec3(x,y,z));
            Layer::setNextDrawable();
        }
        Layer::setLayerPosition(x / getSize()._x, y / getSize()._y, z);
    };
    void TileGroup::changeGroupPosition(float x, float y, float z)
    {
        Layer::changeLayerPosition(x, y, z);

        Drawable* drawable;
        Layer::setFirstDrawable();
        while(Layer::getDrawable() != nullptr)
        {
            drawable = Layer::getDrawable();
            drawable->changePosition(math::Vec3(x,y,z));
            Layer::setNextDrawable();
        }
    };

    void TileGroup::bindToLayer(Layer *layer)
    {
        _layer = layer;
        layer->addGroup(this);
        _bound = 1;
    };

    void TileGroup::unbindFromLayer()
    {
        removeDrawables();
        _layer = nullptr;
        _bound = 0;
    };

    void TileGroup::removeDrawables()
    {
        Layer::setFirstDrawable();
        while(Layer::getDrawable() != nullptr)
        {
            _layer->removeDrawable(Layer::getDrawable());
            Layer::removeDrawable(Layer::getDrawable());
            Layer::setNextDrawable();
        }
    };
    void TileGroup::rebindToLayer()
    {
        _layer->addGroup(this);
        _bound = 1;
    };
}
}
