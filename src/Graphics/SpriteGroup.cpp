#include "SpriteGroup.h"

namespace protosprite
{
namespace graphics
{
SpriteGroup::SpriteGroup(int* tiles, int width, int height,int texId)
    :_spriteTiles(nullptr), _spriteWidth(0), _spriteHeight(0), _texId(texId)
{
    changeTiles(tiles, width, height);
}

SpriteGroup::~SpriteGroup()
{
    free(_spriteTiles);
}

void SpriteGroup::changeTiles(int* tiles, int width, int height)
{
    if(_spriteTiles != nullptr && (width != _spriteWidth && height != _spriteHeight))
    {
        free(_spriteTiles);
        _spriteTiles = (int*)calloc(1, width * height * sizeof(int));
        _spriteWidth = width;
        _spriteHeight = height;
    }
    for(int i = 0; i < width * height; i++)
    {
        _spriteTiles[i] = tiles[i];
    }
    TileGroup::removeDrawables();
    updateDrawables();
    TileGroup::rebindToLayer();
}

void SpriteGroup::updateDrawables()
{
    for(int i = 0; i < _spriteWidth; i++)
        for(int j = 0; j < _spriteHeight; j++)
            if(_spriteTiles[(i * _spriteWidth) + j] != -1)
                TileGroup::addDrawable(new Drawable(Layer::getSize(),
                                                    math::Vec3(i,j,0).add(Layer::getLayerPosition()),
                                                    _texId,
                                                    _spriteTiles[(i * _spriteWidth) + j]));
}

}
}
