#include "Drawable.h"

namespace protosprite
{
namespace graphics
{
    Drawable::Drawable(math::Vec2 size, math::Vec3 position,GLuint texId, int atlasTextureId)
        : _size(size), _position(position), _texId(texId), _colour(math::Vec4(1,1,1,1)), _atlasTextureId(atlasTextureId)
    {
    } ;
    Drawable::Drawable(math::Vec2 size, math::Vec3 position, math::Vec4 colour)
        : _size(size), _position(position), _colour(colour)
    {
        _texId = -1;
        _atlasTextureId = 0;
    } ;
    Drawable::~Drawable()
    {

    };

    void Drawable::setSize(float x, float y)
    {
        _size._x = x;
        _size._y = y;
    };
    void Drawable::setPosition(float x, float y, float z)
    {
        _position._x = x;
        _position._y = y;
        _position._z = z;
    };

    void Drawable::changePosition(math::Vec3 pos)
    {
        _position.add(pos);
    };

    const math::Vec4& Drawable::getColour() const
    {
        return _colour;
    };
    void Drawable::setColour(float r, float g, float b, float a)
    {
        _colour._x = r;
        _colour._y = g;
        _colour._z = b;
        _colour._w = a;
    };

    const GLuint Drawable::getTexId() const
    {
        return _texId;
    };

    const int Drawable::getAtlasTexId() const
    {
        return _atlasTextureId;
    };

    void Drawable::setAtlasTexId(int atlasTextureId)
    {
        _atlasTextureId = atlasTextureId;
    };
}
}
