#pragma once

#include "DrawableList.h"

namespace protosprite
{
namespace graphics
{
class Layer
{
    math::Vec2 _size;
    math::Vec3 _position;
    DrawableList* _drawables;
public:

    Layer(math::Vec2 size, math::Vec3 position);
    ~Layer();
    void setFirstDrawable();
    void setNextDrawable();
    Drawable* getDrawable();
    void addDrawable(Drawable* drawable);
    void removeDrawable(Drawable* drawable);
    void addGroup(Layer* group);
    void setSize(float x, float y);
    math::Vec2& getSize();
    void multiplyLayerSize(math::Vec2 size);
    void setLayerPosition(float x, float y, float z);
    void changeLayerPosition(float x, float y, float z);
    const math::Vec2& getLayerSize() const;
    const math::Vec3& getLayerPosition() const;
};
}
}
