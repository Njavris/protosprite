#include "Texture.h"

namespace protosprite
{
namespace graphics
{
    Texture::Texture(const unsigned char* image, int width, int height, int size)
        :_width(width), _height(height)
    {
        _image = (unsigned char*)malloc(size * sizeof(unsigned char));
        _filename = nullptr;
        memcpy(_image,image, size);
    };

    Texture::Texture(const char* filename)
    {
        _filename = (char*)malloc((strlen(filename) + 1) * sizeof(char));
        strcpy(_filename, filename);
        loadImage();
    };

    Texture::~Texture()
    {
        unload();
    };

    void Texture::load()
    {
        loadTex();
    };

    void Texture::loadImage()
    {
        if(_filename != nullptr)
        {
            _image = SOIL_load_image(_filename, &_width, &_height, 0, SOIL_LOAD_RGBA);
            utils::DebugLogger *logger = utils::DebugLogger::getInstance();
            logger->logMessage("Texture",_filename,1);
        }
        else
        {
            utils::DebugLogger *logger = utils::DebugLogger::getInstance();
            logger->logMessage("Texture","Memory",1);
        }
    };

    void Texture::loadTex()
    {
        glGenTextures(1, &_texID);
        glBindTexture(GL_TEXTURE_2D, _texID);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, _width, _height, 0, GL_RGBA, GL_UNSIGNED_BYTE, _image);
        glBindTexture(GL_TEXTURE_2D, 0);
    };

    void Texture::unload()
    {
        SOIL_free_image_data(_image);
    };

    void Texture::texBind()
    {
        glBindTexture(GL_TEXTURE_2D, _texID);
    };

    void Texture::texUnbind()
    {
        glBindTexture(GL_TEXTURE_2D, 0);
    };

    const unsigned char* Texture::getImage() const
    {
        return _image;
    };
    const int Texture::getWidth() const
    {
        return _width;
    };
    const int Texture::getHeight() const
    {
        return _height;
    };
    const GLuint Texture::getTexId() const
    {
        return _texID;
    };
}
}
