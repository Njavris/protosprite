#pragma once

#include "../Utils/defines.h"
#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include "../Utils/DebugLogger.h"
#include "../Input/InputStack.h"

#define KB_KEYS 1024
#define M_BUTTONS 16

using namespace protosprite::input;

namespace protosprite
{
namespace graphics
{
class Window
{
    utils::DebugLogger *_logger;
    GLFWwindow *_window;
    int _width;
    int _height;
    const char *_title;

    int Init();
    friend void mouseButtonCallback(GLFWwindow *window, int button, int action, int mods);
    friend void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods);
    friend void mousePositionCallback(GLFWwindow *window, double xpos, double ypos);
    friend void GLErrorCallback(int error, const char* description);
public:
    Window(const char *title, int width, int height);
    ~Window();
    void Clear() const;
    void Update();
    bool isOpen() const;
    void setWidthHeight(int width, int height);
    void Close();
    const int getWidth()const;
    const int getHeight()const;
};
}
}
