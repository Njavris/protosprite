#pragma once

#include <GL\glew.h>

namespace protosprite
{
namespace graphics
{
class IndexBuffer
{
    GLuint _buffID;
    GLuint _size;

public:
    IndexBuffer(GLushort *data, GLsizei size);
    ~IndexBuffer();
    void bind() const;
    void unbind() const;
    GLuint getSize() const;
};
}
}
