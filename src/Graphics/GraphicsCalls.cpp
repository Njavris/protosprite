#include "GraphicsCalls.h"
namespace protosprite
{
namespace graphics
{
GraphicsCalls *GraphicsCalls::_instance = nullptr;

GraphicsCalls* GraphicsCalls::getInstance()
{
    if(_instance == 0) _instance = new GraphicsCalls();
    return _instance;
}

GraphicsCalls::GraphicsCalls()
{
    initGraphics();
}

GraphicsCalls::GraphicsCalls(int width, int height, int texArraySize, int screenWidth, int screenHeight, int screenDepth,
                int windowWidth, int windowHeight, int defaultAtlasWidth, int defaultAtlasHeight)
{
    _instance = this;
    _drawMngr = new Drawer(width,
                               height,
                               texArraySize,
                               screenWidth,
                               screenHeight,
                               screenDepth,
                               windowWidth,
                               windowHeight,
                               defaultAtlasWidth,
                               defaultAtlasHeight);
}

int GraphicsCalls::initGraphics()
{
    //should come from config
    int textureSize = 32;
    int letterSize = 16;
    int screenWidth = 28;
    int screenHeight = 18;
    int screenDepth = 2;
    int screenXReso = 896;
    int screenYReso = 504;
    int atlasWidth = 32;
    int texArraySize = 8;

    _drawMngr = new Drawer(textureSize,
                               textureSize,
                               texArraySize,
                               screenWidth,
                               screenHeight,
                               screenDepth,
                               screenXReso,
                               screenYReso,
                               atlasWidth,
                               atlasWidth);
}

const int GraphicsCalls::generateLayer(math::Vec3 position, int* layerSize, int* tiles, int atlasId)
{
    int layer = addLayer(math::Vec2(1,1), position);
    for(int i = layerSize[0] - 1; i >= 0; --i)
    {
        for(int j = layerSize[1] - 1; j >= 0; --j)
        {
            if(tiles[j * layerSize[0] + i] != -1)
                addTileToLayer(layer, math::Vec2(1,1), math::Vec3(i,layerSize[1] - j - 1,0), atlasId, tiles[j * layerSize[0] + i] + 1);
        }
    }
    return layer;
}

const int GraphicsCalls::fillAtlas(int atlasWidth, int textureWidth, int fileCount, const char** filenames, int **imageIds)
{
    *imageIds = (int*)calloc(1,fileCount * sizeof(int));
    int atlasId = addEmptyAtlas(atlasWidth, atlasWidth, textureWidth, textureWidth);

    for(int i = 0; i < fileCount; i++)
        (*imageIds)[i] = addToAtlas(atlasId,filenames[i]);

    return atlasId;
}


}
}
