#pragma once

#include <GL/glew.h>

#include "../Utils/DebugLogger.h"
#include "../Math/Math.h"
#include "../Utils/FileUtils.h"

namespace protosprite
{
namespace graphics
{
class Shader
{
    utils::DebugLogger *_logger;
    const char *_vertPath;
    const char *_fragPath;

    GLuint load();
public:
    GLuint shaderID;
    Shader(const char *vertPath, const char *fragPath);
    ~Shader();
    void enable() const;
    void disable() const;
    GLint getUniformLocation(const GLchar *name);
    void setUniform1i(const GLchar *name, int integer);
    void setUniform1iv(const GLchar *name, int *integer, int count);
    void setUniform1uiv(const GLchar *name, unsigned int *integer, int count);
    void setUniform1f(const GLchar *name, float number);
    void setUniform1fv(const GLchar *name, float *number, int count);
    void setUniformVec2(const GLchar *name, const math::Vec2 &vector);
    void setUniformVec3(const GLchar *name, const math::Vec3 &vector);
    void setUniformVec4(const GLchar *name, const math::Vec4 &vector);
    void setUniformMat4(const GLchar *name, const math::Mat4 &matrix);
    void setUniform2fv(const GLchar *name, GLfloat *value, int size);
};
}
}
