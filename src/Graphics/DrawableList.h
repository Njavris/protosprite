#pragma once

#include <list>
#include <algorithm>

#include "Drawable.h"
#include "Letter.h"

namespace protosprite
{
namespace graphics
{
class DrawableList
{
    std::list<Drawable*> *_drawables;
    std::list<Drawable*>::iterator _drawableIt;
public:
    DrawableList();
    ~DrawableList();
    void setFirstDrawable();
    void setNextDrawable();
    Drawable* getDrawable();
    void addDrawable(Drawable* drawable);
    void removeDrawable(Drawable* drawable);
    bool doesExist(Drawable* drawable);
};
}
}
