#pragma once

#include "TileGroup.h"

namespace protosprite
{
namespace graphics
{
class TextGroup : public TileGroup
{
    char* _string;
    int _len;
    GLuint _texId;
public:
    TextGroup(char* string, int len, GLuint texId);
    void setString(char* str, int len);
    int getString(char* outStr);
protected:
    void updateStringDrawables();
};
}
}
