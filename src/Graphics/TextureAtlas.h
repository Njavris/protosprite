#pragma once

#include <cstdlib>
#include <cstring>

namespace protosprite
{
namespace graphics
{
class TextureAtlas
{
    unsigned char* _image;
    int _texWidth;
    int _texHeight;
    int _atlasWidth;
    int _atlasHeight;
    int _lastPos;

    //int addToAtlas(unsigned char* atlas, int atlasWidLen, int widLen, int lastpos, const unsigned char* image);
    int addToAtlas(unsigned char* atlas, int atlasWidth, int atlasHeight, int width, int height, int lastpos, const unsigned char* image);
public:
    TextureAtlas(int atlasWidth, int texWidth);
    TextureAtlas(int atlasWidth, int atlasHeight, int texWidth, int texHeight);
    ~TextureAtlas();
    const int addTexture(const unsigned char* texture);
    void setTexture(const unsigned char* texture);
    const int getAtlasWidth() const;
    const int getAtlasHeight() const;
    const int getAtlasWidthPx() const;
    const int getAtlasHeightPx() const;
    const int getAtlasSize() const;
    const int getTextureWidth() const;
    const int getLastPos() const;
    const unsigned char* getAtlas() const;
};
}
}
