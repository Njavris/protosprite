#include "DrawableList.h"

namespace protosprite
{
namespace graphics
{
    DrawableList::DrawableList()
    {
        _drawables = new std::list<Drawable*>();
        _drawableIt = _drawables->begin();
    };

    DrawableList::~DrawableList()
    {
        delete(_drawables);
    };

    void DrawableList::setFirstDrawable()
    {
        _drawableIt = _drawables->begin();
    };

    void DrawableList::setNextDrawable()
    {
        _drawableIt++;
    };

    Drawable* DrawableList::getDrawable()
    {
        if(_drawableIt == _drawables->end())
            return nullptr;
        else
            return *_drawableIt;
    };

    void DrawableList::addDrawable(Drawable* drawable)
    {
        _drawables->insert(_drawables->begin(),drawable);
    };

    void DrawableList::removeDrawable(Drawable* drawable)
    {
        _drawables->remove(drawable);
    };

    bool DrawableList::doesExist(Drawable* drawable)
    {
        return (std::find(_drawables->begin(),_drawables->end(), drawable) != _drawables->end());
    };
}
}
