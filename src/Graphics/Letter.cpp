#include "Letter.h"

namespace protosprite
{
namespace graphics
{
    Letter::Letter(math::Vec2 size, math::Vec3 position,GLuint texId, char letter)
        :Drawable(size,position,texId, (int)((int)letter + LETTER_OFFSET)), _letter(letter)
    {

    };

    void Letter::setLetter(char letter)
    {
        Drawable::setAtlasTexId((int)(letter + LETTER_OFFSET));
        _letter = letter;
    };

    char Letter::getLetter()
    {
      return _letter;
    };
}
}
