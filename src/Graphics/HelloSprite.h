#pragma once

#include <vector>
#include "../Math/Math.h"
#include "Texture.h"
#include "Shader.h"

namespace protosprite
{
namespace graphics
{
class HelloSprite
{
    float _posX;
    float _posY;
    std::vector<Texture*> *_textures;
    int _activeTexture;
    std::vector<GLuint> *_texID;
public:
    HelloSprite(const char* filename, math::Vec2 position)
    {
        _textures = new std::vector<Texture*>();
        _texID = new std::vector<GLuint>();
        addTexture(filename);
        _posX = position._x;
        _posY = position._y;
    }

    ~HelloSprite()
    {
        delete(_textures);
        delete(_texID);
    }
    inline void setPositionX(float x)
    {
        _posX = x;
    };
    inline void setPositionY(float y)
    {
        _posY = y;
    };
    inline float getPositionY()
    {
        return _posY;
    };
    inline float getPositionX()
    {
        return _posX;
    };
    //const &math::Vec2 getPosition() const {return math::Vec2(_posX, _posY);};
    inline void setActiveTexture(int actTexture)
    {
        _activeTexture = actTexture;
    };
    inline const int getActiveTexture() const
    {
        return _activeTexture;
    };

    const int addTexture(const char* filename)
    {
        GLuint ID;
        _textures->push_back(new Texture(filename));
        glGenTextures(1, &ID);
        glBindTexture(GL_TEXTURE_2D, ID);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, _textures->back()->getWidth(), _textures->back()->getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, _textures->back()->getImage());
        glBindTexture(GL_TEXTURE_2D, 0);

        _texID->push_back(ID);
        return _textures->size() - 1;
    };

    void Draw(GLuint VAO, Shader *shader)
    {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, _texID->back());
        shader->setUniform1i("ourTexture",0);
        shader->setUniformMat4("_projection",math::Mat4::orthographic(-2,2,-1.5f,1.5f,-1,1));
        shader->setUniformMat4("_translation",math::Mat4::translation(math::Vec3(_posX,_posY,0)));
        //shader->setUniformMat4("_rotation",math::Mat4::rotation(glfwGetTime()*50.0f, math::Vec3(0,0,1)));
        //shader->setUniformMat4("_scale",math::Mat4::scale(math::Vec3(2,2,0)));
        glBindVertexArray(VAO);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
        glBindVertexArray(0);
    };
};
}
}
