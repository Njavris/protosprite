#include "IndexBuffer.h"

namespace protosprite
{
namespace graphics
{
    IndexBuffer::IndexBuffer(GLushort *data, GLsizei size)
        :_size(size)
    {
        glGenBuffers(1, &_buffID);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _buffID);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, _size * sizeof(GLushort), data, GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }

    IndexBuffer::~IndexBuffer()
    {
        glDeleteBuffers(1, &_buffID);
    }

    void IndexBuffer::bind() const
    {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _buffID);
    }

    void IndexBuffer::unbind() const
    {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }

    GLuint IndexBuffer::getSize() const
    {
        return _size;
    };
}
}
