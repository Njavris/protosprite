#include "TextureList.h"

namespace protosprite
{
namespace graphics
{
    TextureList::TextureList()
    {
        _textures = new std::map<GLuint,Texture*>();
    };

    TextureList::~TextureList()
    {
        delete(_textures);
    };

    const Texture* TextureList::getTexture(GLuint textureNr) const
    {
        try
        {
            return _textures->at(textureNr);
        }
        catch(const std::out_of_range &outOfRange)
        {
            return nullptr;
        }
    }

    const GLuint TextureList::addTexture(const char* filename)
    {
        Texture *tmp = new Texture(filename);
        tmp->load();
        _textures->insert(std::pair<GLuint,Texture*>(tmp->getTexId(),tmp));
        return tmp->getTexId();
    }

    const GLuint TextureList::addTexture(const unsigned char* image, int width, int height, int size)
    {
        Texture *tmp = new Texture(image,width,height,size);
        tmp->load();
        _textures->insert(std::pair<GLuint,Texture*>(tmp->getTexId(),tmp));
        return tmp->getTexId();
    }

    void TextureList::unloadAll()
    {
        std::map<GLuint,Texture*>::iterator it;
        for (it = _textures->begin(); it != _textures->end(); it++)
        {
            (it->second)->unload();
        }
    }

    void TextureList::loadAll()
    {
        std::map<GLuint,Texture*>::iterator it;
        for (it = _textures->begin(); it != _textures->end(); it++)
        {
            (it->second)->load();
        }
    }

    const unsigned int TextureList::getSize()
    {
        return _textures->size();
    }

    GLuint TextureList::getFirst()
    {
        _texIter = _textures->begin();
        return (_texIter->first);
    }

    GLuint TextureList::getNext()
    {
        _texIter++;
        return _texIter->first;
    }
}
}
