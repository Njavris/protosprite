#include "Drawer.h"

namespace protosprite
{
namespace graphics
{
    Drawer::Drawer(int width, int height, int texArraySize, int screenWidth, int screenHeight, int screenDepth,
                             int windowWidth, int windowHeight, int defaultAtlasWidth, int defaultAtlasHeight)
        :_tileWidth(width), _tileHeight(height), _texArraySize(texArraySize),
          _screenWidth(screenWidth), _screenHeight(screenHeight), _screenDepth(screenDepth),
          _windowWidth(windowWidth), _windowHeight(windowHeight),
          _atlassWidth(defaultAtlasWidth), _atlassHeight(defaultAtlasHeight)
    {
        init();
    }

    void Drawer::init()
    {
        _layers = new LayerList();
        _atlas = (TextureAtlas**)calloc(1,_texArraySize * sizeof(TextureAtlas*));
        _texIDs = (GLint*)calloc(1,_texArraySize * sizeof(GLint));
        _texId = (GLuint*)calloc(1,_texArraySize * sizeof(GLuint));
        _logger = utils::DebugLogger::getInstance();
        _logger->logMessage("Game","Starting application",1);
        _window = new Window("Test1", _windowWidth, _windowHeight);
        _shader = new Shader("src\\Graphics\\Shaders\\Vertex.vert"
                            ,"src\\Graphics\\Shaders\\Fragment.frag");
        _textureList = new TextureList();
        _renderer = new Renderer();
    }

    const int Drawer::addToAtlas(int atlasID, const char* filename)
    {
        int positionID;
        if(atlasID > _texArraySize || _atlas[atlasID] == nullptr)
        {
            return -1;
        }
        Texture* tex = new Texture(filename);
        positionID = _atlas[atlasID]->addTexture(tex->getImage());
        delete tex;
        return positionID;
    }

    const int Drawer::addEmptyAtlas(int atlasWidth, int atlasHeight, int texWidth, int texHeight)
    {
        int atlasID = 0;
        while(atlasID < _texArraySize)
        {
            if(_atlas[atlasID] == nullptr)
            {
                _atlas[atlasID] = new TextureAtlas(atlasWidth, atlasHeight, texWidth, texHeight);
                return atlasID;
            }
            atlasID++;
        }
        return -1;
    }

    const int Drawer::addAtlas(const char* filename, int atlasWidth, int atlasHeight, int texWidth, int texHeight)
    {
        int atlasID = addEmptyAtlas(atlasWidth, atlasHeight, texWidth, texHeight);

        if(atlasID == -1) return -1;

        Texture* tex = new Texture(filename);
        _atlas[atlasID]->setTexture(tex->getImage());
        //_textureList->addTexture(tex->getImage(),tex->getWidth(),tex->getHeight(), tex->getHeight() * tex->getWidth() * 4);
        delete tex;
        return atlasID;
    }

    const int Drawer::addLayer(Layer* layer)
    {
        return _layers->addLayer(layer);
    }

    void Drawer::addDrawableToLayer(int layerId, Drawable* drawable)
    {
        _layers->getLayer(layerId)->addDrawable(drawable);
    }

    void Drawer::bindGroupToLayer(int layerId, TileGroup* group)
    {
        group->bindToLayer(_layers->getLayer(layerId));
    }

    void Drawer::unbindGroupFromLayer(TileGroup* group)
    {
        group->unbindFromLayer();
    }

    void Drawer::setLayerPosition(int layerId,float x, float y, float z)
    {
        _layers->getLayer(layerId)->setLayerPosition(x,y,z);
    }

    void Drawer::changeLayerPosition(int layerId,float x, float y, float z)
    {
        _layers->getLayer(layerId)->changeLayerPosition(x,y,z);
    }

    void Drawer::addTileToLayer(int layerId, math::Vec2 size, math::Vec3 position,int atlasId, int atlasTextureId)
    {
        addDrawableToLayer(layerId, new Drawable(size,
                                                 position,
                                                 _texId[atlasId],
                                                 atlasTextureId));
    }

    void Drawer::commitLayers()
    {
        int nr = _layers->getFirst();
        if(nr == -1) return;
        while(nr != -1)
        {
            _renderer->addLayer(_layers->getLayer(nr));
            nr = _layers->getNext();
        }
    }

    void Drawer::commitTextures()
    {
        for(int i = 0; i < _texArraySize; i ++)
        {
            if(_atlas[i] == nullptr) break;

            _textureList->addTexture(_atlas[i]->getAtlas(),_atlas[i]->getAtlasWidthPx(),_atlas[i]->getAtlasHeightPx(),_atlas[i]->getAtlasSize());
            _texIDs[i] = i;
            if(i == 0)_texId[i] = _textureList->getFirst();
            else _texId[i] = _textureList->getNext();
        }
    }

    void Drawer::setUniforms()
    {
        _shader->enable();
        _shader->setUniform1iv("_textures", _texIDs, _texArraySize);
        math::Mat4 projection = math::Mat4::orthographic(0,_screenWidth,0,_screenHeight,0,_screenDepth);
        _shader->setUniformMat4("_projection", projection);

        GLfloat* atlasSize;
        atlasSize = (GLfloat*)malloc(_texArraySize * sizeof(GLfloat*) * 2);
        for(int i = 0; i < _texArraySize; i ++)
        {
            if(_atlas[i] == nullptr) continue;

            atlasSize[i * 2] = _atlas[i]->getAtlasWidth();
            atlasSize[i * 2 + 1] = _atlas[i]->getAtlasHeight();
        }
        _shader->setUniform2fv("_atlasSize",atlasSize,_texArraySize);
        _renderer->setTexList(_textureList);
    }

    void Drawer::drawFrame()
    {
        _window->Clear();
        _renderer->sendVertexData();
        commitLayers();
        _renderer->commitVertexData();
        _renderer->Draw();
        _shader->disable();
        _shader->enable();
        _window->Update();
    }
}
}
