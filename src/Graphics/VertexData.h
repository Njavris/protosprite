#pragma once

#include "../Math/Math.h"

namespace protosprite
{
namespace graphics
{
struct VertexData
{
    math::Vec3 _vertex;
    math::Vec2 _texCoord;
    float _textureID;
    float _texturePos;
    unsigned int _colour;
};
}
}
