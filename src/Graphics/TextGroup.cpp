#include "TextGroup.h"

namespace protosprite
{
namespace graphics
{
    TextGroup::TextGroup(char* string, int len, GLuint texId)
        :_len(len), _texId(texId)
    {
        _string = (char*)calloc(1,(len + 1) * sizeof(char));
        memcpy(_string, string, len);
        updateStringDrawables();
    };

    void TextGroup::setString(char* str, int len)
    {
        if(!isBound())
            return;
        free(_string);
        _string = (char*)calloc(1,(len + 1) * sizeof(char));
        _len = len;
        memcpy(_string, str, len);
        TileGroup::removeDrawables();
        updateStringDrawables();
        TileGroup::rebindToLayer();
    };

    int TextGroup::getString(char* outStr)
    {
        outStr = _string;
        return _len;
    };

    void TextGroup::updateStringDrawables()
    {
        for(int i = 0; i < _len; i++)
        {
            //math::Vec3 tmp = Layer::getLayerPosition(); //Asshole modifier
            //tmp.divide(math::Vec3(Layer::getSize()._x,Layer::getSize()._y,1));
            TileGroup::addDrawable(new Letter(Layer::getSize(), math::Vec3(i,0,0).add(Layer::getLayerPosition()),_texId,_string[i]));
        }
    };
}
}
