#version 330 core
//layout(location = 0) out vec4 color;

uniform sampler2D _textures[8];

in DATA
{
    vec4 position;
    vec2 texCoord;
    float texID;
    vec4 color;
    float ZoomX;
    float ZoomY;
} fs_in;


void main()
{
    vec4 texColor = fs_in.color;

    if(fs_in.texID > 0.0)
    {
        //texColor = fs_in.color * texture(_textures[int(fs_in.texID - 0.5)], fs_in.texCoord);
    }
    //color =texColor;
    gl_FragColor = texture(_textures[int(fs_in.texID - 0.5)], (fs_in.texCoord) * vec2(fs_in.ZoomX,fs_in.ZoomY));
}

