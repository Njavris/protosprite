#version 330 core

layout (location = 0) in vec4 position;
layout (location = 1) in vec2 texCoord;
layout (location = 2) in float texID;
layout (location = 3) in float atlasPosID;
layout (location = 4) in vec4 color;

uniform mat4 _projection;// = mat4(1.0f);

uniform vec2 _atlasSize[8];

out DATA
{
    vec4 position;
    vec2 texCoord;
    float texID;
    vec4 color;
    float ZoomX;
    float ZoomY;
} vs_out;

void main()
{
    gl_Position = _projection * position;
    vs_out.position  = position;
    vs_out.texID = texID;
    vs_out.color = color;

    vs_out.texCoord.x = texCoord.x + float(mod(int(atlasPosID - 0.5), _atlasSize[int(texID - 0.5)].x));
    vs_out.texCoord.y = 1.0f - texCoord.y + float(floor(int(atlasPosID - 0.5) / _atlasSize[int(texID - 0.5)].x));
    vs_out.ZoomX = (1.0f / _atlasSize[int(texID - 0.5)].x);
    vs_out.ZoomY = (1.0f / _atlasSize[int(texID - 0.5)].y);
}
