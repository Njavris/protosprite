#include "Renderer.h"

namespace protosprite
{
namespace graphics
{
    Renderer::Renderer()
    {
        init();
    }

    Renderer::~Renderer()
    {
        glDeleteBuffers(1, &_VBO);
        delete(_indxBuff);
    }

    void Renderer::init()
    {
        _indiceCount = 0;
        //gen
        glGenVertexArrays(1, &_VAS);
        glGenBuffers(1, &_VBO);
        //bind
        glBindVertexArray(_VAS);
        glBindBuffer(GL_ARRAY_BUFFER, _VBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(VertexData) * 4 * RENDERER_ARR_BUFF_ITEMS, NULL, GL_DYNAMIC_DRAW);
        //enable
        glEnableVertexAttribArray(SHADER_VERTEX_LAYOUT);
        glEnableVertexAttribArray(SHADER_TEXTURE_COORD_LAYOUT);
        glEnableVertexAttribArray(SHADER_TEXTURE_ID_LAYOUT);
        glEnableVertexAttribArray(SHADER_COLOR_LAYOUT);
        glEnableVertexAttribArray(SHADER_TEXTURE_ATLASS_POSITION);
        //pointers

        glVertexAttribPointer(SHADER_VERTEX_LAYOUT, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const GLvoid*)0);
        glVertexAttribPointer(SHADER_TEXTURE_COORD_LAYOUT, 2, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const GLvoid*)(offsetof(VertexData, VertexData::_texCoord)));
        glVertexAttribPointer(SHADER_TEXTURE_ID_LAYOUT, 1, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const GLvoid*)(offsetof(VertexData, VertexData::_textureID)));
        glVertexAttribPointer(SHADER_TEXTURE_ATLASS_POSITION, 1, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const GLvoid*)(offsetof(VertexData, VertexData::_texturePos)));
        glVertexAttribPointer(SHADER_COLOR_LAYOUT, 4, GL_UNSIGNED_INT, GL_TRUE, sizeof(VertexData), (const GLvoid*)(offsetof(VertexData, VertexData::_colour)));
        //test!!!
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        GLushort indices[RENDERER_ARR_BUFF_ITEMS * 6];

        int j = 0;
        for (int i = 0; i < RENDERER_ARR_BUFF_ITEMS * 6; i += 6)
        {
            indices[i + 0] = j + 0;
            indices[i + 1] = j + 1;
            indices[i + 2] = j + 2;
            indices[i + 3] = j + 2;
            indices[i + 4] = j + 3;
            indices[i + 5] = j + 0;
            j += 4;
        }
        _indxBuff = new IndexBuffer(indices, RENDERER_ARR_BUFF_ITEMS * 6);
        glBindVertexArray(0);
    }

    void Renderer::setTexList(TextureList *texList)
    {
        _texList = texList;
    };

    void Renderer::sendVertexData()
    {
        glBindBuffer(GL_ARRAY_BUFFER, _VBO);
        _vertBuff = (VertexData*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
    }

    void Renderer::commitVertexData()
    {
        glUnmapBuffer(GL_ARRAY_BUFFER);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }

    void Renderer::Draw()
    {
        GLuint texID = _texList->getFirst();
        for (int i = 0; i < _texList->getSize(); i++)
        {
            glActiveTexture(GL_TEXTURE0 + i);
            glBindTexture(GL_TEXTURE_2D, texID);
            texID = _texList->getNext();
        }

        glBindVertexArray(_VAS);
        _indxBuff->bind();
        glDrawElements(GL_TRIANGLES, _indiceCount, GL_UNSIGNED_SHORT, NULL);
        _indxBuff->unbind();
        glBindVertexArray(0);
        _indiceCount = 0;
    }

    void Renderer::addDrawable(const Drawable *drawable,const math::Vec3 position, const math::Vec2 size)
    {
        math::Vec3 drawablePos = (drawable->getPosition());
        math::Vec2 drawableSize = (drawable->getSize());
        (drawablePos.add(position)).multiply(math::Vec3(drawableSize._x, drawableSize._y,0));
        drawableSize.multiply(size);

        if((drawablePos._x * drawableSize._x) > 48.0f + 1 ||
           (drawablePos._y * drawableSize._y) > 27.0f + 1 ||
           (drawablePos._x * drawableSize._x) < 0.0f - 1 ||
           (drawablePos._y * drawableSize._y) < 0.0f - 1)
            return;

        int r = drawable->getColour()._x * 255.0f;
        int g = drawable->getColour()._y * 255.0f;
        int b = drawable->getColour()._z * 255.0f;
        int a = drawable->getColour()._w * 255.0f;
        unsigned int colour = a << 24 | b << 16 | g << 8 | r;

        _vertBuff->_colour = colour;
        _vertBuff->_textureID = drawable->getTexId();
        _vertBuff->_texturePos = drawable->getAtlasTexId();
        _vertBuff->_texCoord = math::Vec2(0,0);
        _vertBuff->_vertex = math::Mat4::identity() * drawablePos;
        _vertBuff++;
        _vertBuff->_colour = colour;
        _vertBuff->_textureID = drawable->getTexId();
        _vertBuff->_texturePos = drawable->getAtlasTexId();
        _vertBuff->_texCoord = math::Vec2(0,1);
        _vertBuff->_vertex = math::Mat4::identity() * math::Vec3(drawablePos._x,
                             drawablePos._y + drawableSize._y,
                             drawablePos._z);
        _vertBuff++;
        _vertBuff->_colour = colour;
        _vertBuff->_textureID = drawable->getTexId();
        _vertBuff->_texturePos = drawable->getAtlasTexId();
        _vertBuff->_texCoord = math::Vec2(1,1);
        _vertBuff->_vertex = math::Mat4::identity() * math::Vec3(drawablePos._x + drawableSize._x,
                             drawablePos._y + drawableSize._y,
                             drawablePos._z);
        _vertBuff++;
        _vertBuff->_colour = colour;
        _vertBuff->_textureID = drawable->getTexId();
        _vertBuff->_texturePos = drawable->getAtlasTexId();
        _vertBuff->_texCoord = math::Vec2(1,0);
        _vertBuff->_vertex = math::Mat4::identity() * math::Vec3(drawablePos._x + drawableSize._x,
                             drawablePos._y,
                             drawablePos._z);
        _vertBuff++;
        _indiceCount += 6;
    }

    void Renderer::addLayer(Layer *layer)
    {
        layer->setFirstDrawable();
        while(layer->getDrawable() != nullptr)
        {
            //(layer->getDrawable())->changePosition(layer->getLayerPosition());
            addDrawable(layer->getDrawable(),layer->getLayerPosition(), layer->getLayerSize());
            //addDrawable(layer->getDrawable(), math::Vec3(0,0,0), math::Vec2(1,1));
            layer->setNextDrawable();
        }
    }
}
}
