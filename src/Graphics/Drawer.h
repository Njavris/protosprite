/*
Encapsulates all drawing functionality
*/
#pragma once

#include "Window.h"
#include "Renderer.h"
#include "Shader.h"
#include "LayerList.h"
#include "TextureList.h"
#include "TextureAtlas.h"
#include "TileGroup.h"
#include "TextGroup.h"
#include "../Utils/DebugLogger.h"

//drawing and rendering
namespace protosprite
{
namespace graphics
{
class Drawer
{
    TextureList *_textureList;
    Renderer *_renderer;
    GLint* _texIDs; //only for shader
    Shader *_shader; //single common shader for now
    utils::DebugLogger *_logger;
    TextureAtlas **_atlas;
    LayerList *_layers;
    int _screenWidth;
    int _screenHeight;
    int _screenDepth;
    int _tileWidth;
    int _tileHeight;
    int _texCount;
    int _atlassWidth;
    int _atlassHeight;
    int _texArraySize;
    int _windowWidth;
    int _windowHeight;
    void init();
public:
    GLuint* _texId; //actual texture id
    Window *_window; //Input will use this
// ^ these should be private
    Drawer(int width, int height, int texArraySize, int screenWidth, int screenHeight, int screenDepth,
                int windowWidth, int windowHeight, int defaultAtlasWidth, int defaultAtlasHeight);
    const int addToAtlas(int atlasID, const char* filename);
    const int addAtlas(const char* filename, int atlasWidth, int atlasHeight, int texWidth, int texHeight);
    const int addEmptyAtlas(int atlasWidth, int atlasHeight, int texWidth, int texHeight);
    const int addLayer(Layer* layer);
    void addDrawableToLayer(int layerId, Drawable* drawable);
    void bindGroupToLayer(int layerId, TileGroup* group);
    void unbindGroupFromLayer(TileGroup* group);
    void setLayerPosition(int layerId,float x, float y, float z);
    void changeLayerPosition(int layerId,float x, float y, float z);
    void addTileToLayer(int layerId, math::Vec2 size, math::Vec3 position,int atlasId, int atlasTextureId);
    void commitLayers();
    void commitTextures();
    void setUniforms();
    void drawFrame();
};
}
}
