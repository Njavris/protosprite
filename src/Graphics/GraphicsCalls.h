#pragma once
#include "Drawer.h"

namespace protosprite
{
namespace graphics
{
class GraphicsCalls
{
    Drawer* _drawMngr;
    static GraphicsCalls *_instance;
    GraphicsCalls(const GraphicsCalls &cpy);
    GraphicsCalls();
public:
    static GraphicsCalls* getInstance();
    //Should be removed later, when configs are supported
    GraphicsCalls(int width, int height, int texArraySize, int screenWidth, int screenHeight, int screenDepth,
                int windowWidth, int windowHeight, int defaultAtlasWidth, int defaultAtlasHeight);
    int initGraphics();
    void loadResources(const char *filename){};
    void setMeshMode() {glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);};
    void setFillMode() {glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);};
    int isMeshMode()
    {
        GLint polyMode;
        glGetIntegerv(GL_POLYGON_MODE, &polyMode);
        return polyMode == GL_LINE ? 1 : 0;
    };
    int isFillMode()
    {
        GLint polyMode;
        glGetIntegerv(GL_POLYGON_MODE, &polyMode);
        return polyMode == GL_FILL ? 1 : 0;
    };
    const int generateLayer(math::Vec3 position, int* layerSize, int* tiles, int atlasId);
    const int fillAtlas(int atlasWidth, int textureWidth, int fileCount, const char** filenames, int **imageIds);

    const int addEmptyAtlas(int atlasWidth, int atlasHeight, int texWidth, int texHeight)
        {return _drawMngr->addEmptyAtlas(atlasWidth, atlasHeight, texWidth, texHeight);};

    const int addAtlas(const char* filename, int atlasWidth, int atlasHeight, int texWidth, int texHeight)
        {return _drawMngr->addAtlas(filename, atlasWidth, atlasHeight, texWidth, texHeight);};

    const int addToAtlas(int atlasID, const char* filename)
        {return _drawMngr->addToAtlas(atlasID,filename);};

    const int addLayer(math::Vec2 size, math::Vec3 position)
        {
            Layer *lyr = new Layer(size, position);
            return _drawMngr->addLayer(lyr);
        };

    void moveLayer(int layerId,float x, float y, float z)
        {_drawMngr->changeLayerPosition(layerId,x,y,z);};

    void setLayerPosition(int layerId,float x, float y, float z)
        {_drawMngr->setLayerPosition(layerId, x, y, z);};

    void addTileToLayer(int layerId, math::Vec2 size, math::Vec3 position,int atlasId, int atlasTextureId)
        {_drawMngr->addTileToLayer(layerId,size,position,atlasId,atlasTextureId);};

    void commitTextures() {_drawMngr->commitTextures();};
    void commitLayers() {_drawMngr->commitLayers();};
    void bindGroupToLayer(int layerId, TileGroup* group)
        {_drawMngr->bindGroupToLayer(layerId,group);};
    void unbindGroupFromLayer(TileGroup* group)
        {_drawMngr->unbindGroupFromLayer(group);};
    void setUniforms() {_drawMngr->setUniforms();};
    void drawFrame() {_drawMngr->drawFrame();};
    Window *getWindow()
        {return _drawMngr->_window;};
    Drawer *getDrawer()
        {return _drawMngr;};
};
}
}
