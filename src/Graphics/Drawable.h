#pragma once

#include <GL/glew.h>

#include "../Math/Math.h"

namespace protosprite
{
namespace graphics
{
class Drawable
{
    math::Vec2 _size;
    math::Vec3 _position;
    math::Vec4 _colour;
    GLuint _texId;
    int _atlasTextureId;
public:
    Drawable(math::Vec2 size, math::Vec3 position,GLuint texId, int atlasTextureId);
    Drawable(math::Vec2 size, math::Vec3 position, math::Vec4 colour);
    virtual ~Drawable();
    void setSize(float x, float y);
    void setPosition(float x, float y, float z);
    void changePosition(math::Vec3 pos);
    const math::Vec4& getColour() const;
    void setColour(float r, float g, float b, float a);
    const GLuint getTexId() const;
    const int getAtlasTexId() const;
    void setAtlasTexId(int atlasTextureId);
    inline const math::Vec2& getSize() const
    {
        return _size;
    };
    inline const math::Vec3& getPosition() const
    {
        return _position;
    };
};
}
}
