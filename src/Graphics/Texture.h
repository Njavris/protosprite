#pragma once

#include <GL/glew.h>
#include <cstdlib>
#include <cstring>

#include "../SOIL/SOIL.h"
#include "../Utils/DebugLogger.h"

namespace protosprite
{
namespace graphics
{
class Texture
{
    char* _filename;
    int _width, _height;
    GLuint _texID;
    unsigned char* _image;
public:
    Texture(const unsigned char* image, int width, int height, int size);
    Texture(const char* filename);
    ~Texture();
    void load();
    void loadImage();
    void loadTex();
    void unload();
    void texBind();
    void texUnbind();
    const unsigned char* getImage() const;
    const int getWidth() const;
    const int getHeight() const;
    const GLuint getTexId() const;
};
}
}
