#pragma once

#include "Layer.h"

namespace protosprite
{
namespace graphics
{
class TileGroup: public Layer
{
    Layer *_layer;
    int _bound;
public:
    TileGroup();
    ~TileGroup();
    void setGroupPosition(float x, float y, float z);
    void changeGroupPosition(float x, float y, float z);
    void bindToLayer(Layer *layer);
    void unbindFromLayer();
    const int isBound() const {return _bound;};
protected:
    void rebindToLayer();
    void removeDrawables();
};
}
}
