#pragma once
#include "../Utils/Defines.h"

#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include "../Utils/DebugLogger.h"

#include "IndexBuffer.h"
#include "VertexData.h"
#include "TextureList.h"
#include "Layer.h"

namespace protosprite
{
namespace graphics
{
class Renderer
{
    GLuint _VAS;
    GLuint _VBO;
    GLuint _VIO;
    GLsizei _indiceCount;
    TextureList *_texList;
    VertexData *_vertBuff;
    IndexBuffer *_indxBuff;
    math::Mat4 _projection;

public:
    Renderer();
    ~Renderer();
    void init();
    void setTexList(TextureList *texList);
    void sendVertexData();
    void commitVertexData();
    void Draw();
    void addLayer(Layer *layer);
    void addDrawable(const Drawable *drawable, const math::Vec3 position, const math::Vec2 size);
    inline void addDrawable(const Drawable *drawable)
    {
        addDrawable(drawable, math::Vec3(0,0,0), math::Vec2(1,1));
    };
};
}
}
