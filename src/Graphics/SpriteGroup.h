#pragma once
#include "TileGroup.h"

namespace protosprite
{
namespace graphics
{
class SpriteGroup: public TileGroup
{
    int _spriteWidth;
    int _spriteHeight;
    int* _spriteTiles;
    GLuint _texId;
public:
    SpriteGroup(int* tiles, int width, int height, int texId);
    ~SpriteGroup();
    void changeTiles(int* tiles, int width, int height);
    void updateDrawables();
    const int getHeight() const {return _spriteHeight;};
    const int getWidth() const {return _spriteWidth;};
};
}
}
