#pragma once

namespace protosprite
{
namespace math
{
class Vec4
{
public:
    float _x, _y, _z, _w;
    Vec4();
    Vec4(const float &x, const float &y, const float &z, const float &w);
    Vec4& add(const Vec4 &second);
    Vec4& subtract(const Vec4 &second);
    Vec4& multiply(const Vec4 &second);
    Vec4& divide(const Vec4 &second);
    Vec4& operator+=(const Vec4 &second);
    Vec4& operator-=(const Vec4 &second);
    Vec4& operator*=(const Vec4 &second);
    Vec4& operator/=(const Vec4 &second);
    bool operator==(const Vec4 &second);
    bool operator!=(const Vec4 &second);
    friend Vec4 operator+(Vec4 first, const Vec4 &second);
    friend Vec4 operator-(Vec4 first, const Vec4 &second);
    friend Vec4 operator*(Vec4 first, const Vec4 &second);
    friend Vec4 operator/(Vec4 first, const Vec4 &second);
};
}
}
