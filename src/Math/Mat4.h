
#pragma once
#include "Math.h"

namespace protosprite
{
namespace math
{
class Mat4
{
public:
    union
    {
        float _elements[16];
        Vec4 _columns[4];
    };

    Mat4();
    Mat4(float diagonal);
    static Mat4 identity();
    static Mat4 orthographic(float left, float right, float bottom, float top, float near, float far);
    static Mat4 perspective(float FOV, float aspectRatio, float near, float far);
    static Mat4 translation(const Vec3 &translation);
    static Mat4 rotation(float angle, const Vec3 &axis);
    static Mat4 scale(const Vec3 &scale);
    Mat4& multiply(const Mat4 &second);
    Mat4& operator*=(const Mat4 &second);
    friend Mat4 operator*(Mat4 first, const Mat4 &second);
    Vec3 multiply(const Vec3 &second) const;
    Vec4 multiply(const Vec4 &second) const;
    friend Vec3 operator*(const Mat4 &first, const Vec3 &second);
    friend Vec4 operator*(const Mat4 &first, const Vec4 &second);
};
}
}
