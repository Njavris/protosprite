#pragma once

namespace protosprite
{
namespace math
{
class Vec3
{
public:
    float _x, _y, _z;
    Vec3();
    Vec3(const float &x, const float &y, const float &z);
    Vec3& add(const Vec3 &second);
    Vec3& subtract(const Vec3 &second);
    Vec3& multiply(const Vec3 &second);
    Vec3& divide(const Vec3 &second);
    Vec3& operator+=(const Vec3 &second);
    Vec3& operator-=(const Vec3 &second);
    Vec3& operator*=(const Vec3 &second);
    Vec3& operator/=(const Vec3 &second);
    bool operator==(const Vec3 &second);
    bool operator!=(const Vec3 &second);
    friend Vec3 operator+(Vec3 first, const Vec3 &second);
    friend Vec3 operator-(Vec3 first, const Vec3 &second);
    friend Vec3 operator*(Vec3 first, const Vec3 &second);
    friend Vec3 operator/(Vec3 first, const Vec3 &second);
};
}
}
