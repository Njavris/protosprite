#pragma once

namespace protosprite
{
namespace math
{
class Vec2
{
public:
    float _x, _y;
    Vec2();
    Vec2(const float &x,const float &y);
    Vec2& add(const Vec2 &second);
    Vec2& subtract(const Vec2 &second);
    Vec2& multiply(const Vec2 &second);
    Vec2& divide(const Vec2 &second);
    Vec2& operator+=(const Vec2 &second);
    Vec2& operator-=(const Vec2 &second);
    Vec2& operator*=(const Vec2 &second);
    Vec2& operator/=(const Vec2 &second);
    bool operator==(const Vec2 &second);
    bool operator!=(const Vec2 &second);
    friend Vec2 operator+(Vec2 first, const Vec2 &second);
    friend Vec2 operator-(Vec2 first, const Vec2 &second);
    friend Vec2 operator*(Vec2 first, const Vec2 &second);
    friend Vec2 operator/(Vec2 first, const Vec2 &second);
};
}
}
