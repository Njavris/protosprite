#include "Vec2.h"

namespace protosprite
{
namespace math
{
    Vec2::Vec2(): _x(0), _y(0) {}
    Vec2::Vec2(const float &x, const float &y):_x(x), _y(y) {}

    Vec2& Vec2::add(const Vec2 &second)
    {
        _x += second._x;
        _y += second._y;
        return *this;
    }
    Vec2& Vec2::subtract(const Vec2 &second)
    {
        _x -= second._x;
        _y -= second._y;
        return *this;
    }
    Vec2& Vec2::multiply(const Vec2 &second)
    {
        _x *= second._x;
        _y *= second._y;
        return *this;
    }
    Vec2& Vec2::divide(const Vec2 &second)
    {
        _x /= second._x;
        _y /= second._y;
        return *this;
    }

    Vec2& Vec2::operator+=(const Vec2 &second)
    {
        return add(second);
    }

    Vec2& Vec2::operator-=(const Vec2 &second)
    {
        return subtract(second);
    }

    Vec2& Vec2::operator*=(const Vec2 &second)
    {
        return multiply(second);
    }

    Vec2& Vec2::operator/=(const Vec2 &second)
    {
        return divide(second);
    }

    bool Vec2::operator==(const Vec2 &second)
    {
        return _x == second._x && _y == second._y;
    }

    bool Vec2::operator!=(const Vec2 &second)
    {
        return !(*this == second);
    }

    Vec2 operator+(Vec2 first, const Vec2 &second)
    {
        return first.add(second);
    }

    Vec2 operator-(Vec2 first, const Vec2 &second)
    {
        return first.subtract(second);
    }

    Vec2 operator*(Vec2 first, const Vec2 &second)
    {
        return first.multiply(second);
    }

    Vec2 operator/(Vec2 first, const Vec2 &second)
    {
        return first.divide(second);
    }
}
}
