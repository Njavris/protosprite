#pragma once

#define _USE_MATH_DEFINES
#include <math.h>

#include <cstring>

#include "Vec2.h"
#include "Vec3.h"
#include "Vec4.h"
#include "Mat4.h"

namespace protosprite
{
namespace math
{
inline float to_radians(float degrees)
{
    return degrees * (float)M_PI / 180;
};
}
}
