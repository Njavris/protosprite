#include "Vec3.h"

namespace protosprite
{
namespace math
{
    Vec3::Vec3(): _x(0), _y(0), _z(0) {}
    Vec3::Vec3(const float & x, const float & y, const float &z): _x(x), _y(y), _z(z) {}

    Vec3& Vec3::add(const Vec3 &second)
    {
        _x += second._x;
        _y += second._y;
        _z += second._z;
        return *this;
    }

    Vec3& Vec3::subtract(const Vec3 &second)
    {
        _x -= second._x;
        _y -= second._y;
        _z -= second._z;
        return *this;
    }

    Vec3& Vec3::multiply(const Vec3 &second)
    {
        _x *= second._x;
        _y *= second._y;
        _z *= second._z;
        return *this;
    }

    Vec3& Vec3::divide(const Vec3 &second)
    {
        _x /= second._x;
        _y /= second._y;
        _z /= second._z;
        return *this;
    }

    Vec3& Vec3::operator+=(const Vec3 &second)
    {
        return add(second);
    }

    Vec3& Vec3::operator-=(const Vec3 &second)
    {
        return subtract(second);
    }

    Vec3& Vec3::operator*=(const Vec3 &second)
    {
        return multiply(second);
    }

    Vec3& Vec3::operator/=(const Vec3 &second)
    {
        return divide(second);
    }

    bool Vec3::operator==(const Vec3 &second)
    {
        return _x == second._x && _y == second._y && _z == second._z;
    }

    bool Vec3::operator!=(const Vec3 &second)
    {
        return !(*this == second);
    }

    Vec3 operator+(Vec3 first, const Vec3 &second)
    {
        return first.add(second);
    }

    Vec3 operator-(Vec3 first, const Vec3 &second)
    {
        return first.subtract(second);
    }

    Vec3 operator*(Vec3 first, const Vec3 &second)
    {
        return first.multiply(second);
    }

    Vec3 operator/(Vec3 first, const Vec3 &second)
    {
        return first.divide(second);
    }
}
}
