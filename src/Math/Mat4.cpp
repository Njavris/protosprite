#include "Mat4.h"

namespace protosprite
{
namespace math
{
    Mat4::Mat4()
    {
        for (int i = 0; i < 16; i++)
            _elements[i] = 0.0f;
    }

    Mat4::Mat4(float diagonal)
    {
        for (int i = 0; i < 16; i++)
            _elements[i] = 0.0f;

        for (int i = 0; i < 4; i++)
            _elements[i + (i * 4)] = diagonal;
    }

    Mat4 Mat4::identity()
    {
        return Mat4(1.0f);
    }

    Mat4 Mat4::orthographic(float left, float right, float bottom, float top, float near, float far)
    {
        Mat4 result(1.0f);

        result._elements[0 + (0 * 4)] = 2.0f / (right - left);
        result._elements[1 + (1 * 4)] = 2.0f / (top - bottom);
        result._elements[2 + (2 * 4)] = 2.0f / (near - far);
        result._elements[0 + (3 * 4)] = (left + right) / (left - right );
        result._elements[1 + (3 * 4)] = (bottom + top) / (bottom - top);
        result._elements[2 + (3 * 4)] = (far + near) / (far - near);

        return result;
    }

    Mat4 Mat4::perspective(float FOV, float aspectRatio, float near, float far)
    {
        Mat4 result(1.0f);

        result._elements[0 + (0 * 4)] = 1.0f / ((float)tan(to_radians(0.5f * FOV)) * aspectRatio);
        result._elements[1 + (1 * 4)] = 1.0f / (float)tan(to_radians(0.5f * FOV));
        result._elements[2 + (2 * 4)] = (near + far) / (near - far);
        result._elements[3 + (2 * 4)] = -1.0f;
        result._elements[2 + (3 * 4)] = (2.0f * near * far) / (near - far);

        return result;
    }

    Mat4 Mat4::translation(const Vec3 &translation)
    {
        Mat4 result(1.0f);

        result._elements[0 + (3 * 4)] = translation._x;
        result._elements[1 + (3 * 4)] = translation._y;
        result._elements[2 + (3 * 4)] = translation._z;

        return result;
    }

    Mat4 Mat4::rotation(float angle, const Vec3 &axis)
    {
        Mat4 result(1.0f);

        //float radAngl = to_radians(angle);
        float cosAngl = cos(to_radians(angle));
        float sinAngl = sin(to_radians(angle));
        float redCosAngl = 1.0f - cosAngl;

        result._elements[0 + (0 * 4)] = axis._x * redCosAngl + cosAngl;
        result._elements[1 + (0 * 4)] = axis._y * axis._x * redCosAngl + axis._z * sinAngl;
        result._elements[2 + (0 * 4)] = axis._x * axis._z * redCosAngl - axis._y * sinAngl;

        result._elements[0 + (1 * 4)] = axis._x * axis._y * redCosAngl - axis._z * sinAngl;
        result._elements[1 + (1 * 4)] = axis._y * redCosAngl + cosAngl;
        result._elements[2 + (1 * 4)] = axis._y * axis._z * redCosAngl + axis._x * sinAngl;

        result._elements[0 + (2 * 4)] = axis._x * axis._z * redCosAngl + axis._y * sinAngl;
        result._elements[1 + (2 * 4)] = axis._y * axis._z * redCosAngl - axis._x * sinAngl;
        result._elements[2 + (2 * 4)] = axis._z * redCosAngl + cosAngl;
        return result;
    }

    Mat4 Mat4::scale(const Vec3 &scale)
    {
        Mat4 result(1.0f);

        result._elements[0 + (0 * 4)] = scale._x;
        result._elements[1 + (1 * 4)] = scale._y;
        result._elements[2 + (2 * 4)] = scale._z;

        return result;
    }

    Mat4 & Mat4::multiply(const Mat4 &second)
    {
        float result[16];
        for (int y = 0; y < 4; y++)
        {
            for (int x = 0; x < 4; x++)
            {
                float sum = 0.0f;
                for (int e = 0; e < 4; e++)
                {
                    sum += _elements[x +  (e * 4)] * second._elements[e + (y * 4)];
                }
                result[x + (y * 4)] = sum;
            }
        }
        memcpy(_elements, result, 16 * sizeof(float));
        return *this;
    }


    Mat4 & Mat4::operator*=(const Mat4  &second)
    {
        return multiply(second);
    }

    Mat4 operator*(Mat4 first, const Mat4 &second)
    {
        return first.multiply(second);
    }

    Vec3 Mat4::multiply(const Vec3  &second) const
    {

        return Vec3(
                   _columns[0]._x * second._x + _columns[1]._x * second._y + _columns[2]._x * second._z + _columns[3]._x,
                   _columns[0]._y * second._x + _columns[1]._y * second._y + _columns[2]._y * second._z + _columns[3]._y,
                   _columns[0]._z * second._x + _columns[1]._z * second._y + _columns[2]._z * second._z + _columns[3]._z
               );
    }

    Vec4 Mat4::multiply(const Vec4 &second) const
    {
        return Vec4(
                   _columns[0]._x * second._x + _columns[1]._x * second._y + _columns[2]._x * second._z + _columns[3]._x * second._w,
                   _columns[0]._y * second._x + _columns[1]._y * second._y + _columns[2]._y * second._z + _columns[3]._y * second._w,
                   _columns[0]._z * second._x + _columns[1]._z * second._y + _columns[2]._z * second._z + _columns[3]._z * second._w,
                   _columns[0]._w * second._x + _columns[1]._w * second._y + _columns[2]._w * second._z + _columns[3]._w * second._w
               );
    }

    Vec3 operator*(const Mat4 &first, const Vec3 &second)
    {
        return first.multiply(second);
    }

    Vec4 operator*(const Mat4 &first, const Vec4 &second)
    {
        return first.multiply(second);
    }
}
}
