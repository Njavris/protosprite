#include "Vec4.h"

namespace protosprite
{
namespace math
{
    Vec4::Vec4():_x(0),_y(0),_z(0),_w(0) {}
    Vec4::Vec4(const float & x, const float & y, const float &z, const float &w):_x(x),_y(y),_z(z),_w(w) {}

    Vec4& Vec4::add(const Vec4 &second)
    {
        _x += second._x;
        _y += second._y;
        _z += second._z;
        _w += second._w;
        return *this;
    }
    Vec4& Vec4::subtract(const Vec4 &second)
    {
        _x -= second._x;
        _y -= second._y;
        _z -= second._z;
        _w -= second._w;
        return *this;
    }
    Vec4& Vec4::multiply(const Vec4 &second)
    {
        _x *= second._x;
        _y *= second._y;
        _z *= second._z;
        _w *= second._w;
        return *this;
    }
    Vec4& Vec4::divide(const Vec4 &second)
    {
        _x /= second._x;
        _y /= second._y;
        _z /= second._z;
        _w /= second._w;
        return *this;
    }

    Vec4& Vec4::operator+=(const Vec4 &second)
    {
        return add(second);
    }

    Vec4& Vec4::operator-=(const Vec4 &second)
    {
        return subtract(second);
    }

    Vec4& Vec4::operator*=(const Vec4 &second)
    {
        return multiply(second);
    }

    Vec4& Vec4::operator/=(const Vec4 &second)
    {
        return divide(second);
    }

    bool Vec4::operator==(const Vec4 &second)
    {
        return _x == second._x && _y == second._y && _z == second._z && _w == second._w;
    }

    bool Vec4::operator!=(const Vec4 &second)
    {
        return !(*this == second);
    }

    Vec4 operator+(Vec4 first, const Vec4 &second)
    {
        return first.add(second);
    }

    Vec4 operator-(Vec4 first, const Vec4 &second)
    {
        return first.subtract(second);
    }

    Vec4 operator*(Vec4 first, const Vec4 &second)
    {
        return first.multiply(second);
    }

    Vec4 operator/(Vec4 first, const Vec4 &second)
    {
        return first.divide(second);
    }
}
}
