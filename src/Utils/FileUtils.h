#pragma once

#include <cstdlib>
#include <fstream>

namespace protosprite
{
namespace utils
{
class FileUtils
{
public:
    static char* readFile(const char *filePath);
};
}
}
