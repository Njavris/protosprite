#include "DebugLogger.h"

namespace protosprite
{
namespace utils
{
    DebugLogger *DebugLogger::_instance = nullptr;

    DebugLogger* DebugLogger::getInstance()
    {
        if(!_instance) _instance = new DebugLogger();
        return _instance;
    };

    void DebugLogger::logMessage(const char* source, const char* msg, int level)
    {
        if(level <= _level || _level == 0)
        {
            time_t mTime = time(nullptr);
            char buff[64];

            FILE *fl = fopen(_filename, "ab");
            strftime(buff,80,"%x %X", localtime(&mTime));
            fprintf(fl,"[%s] %s: %s (%d)\r\n",buff,source,msg,level);
    #ifdef PRINT_LOG
            printf("[%s] %s: %s (%d)\r\n",buff,source,msg,level);
    #endif // PRINT_LOG
            fclose(fl);
        }
    };
    void DebugLogger::setLevel(int lvl)
    {
        _level = lvl;
    };
    const int DebugLogger::getLevel() const
    {
        return _level;
    };
    void DebugLogger::setFilename(const char* filename)
    {
        _filename = filename;
    };
    const char* DebugLogger::getFilename() const
    {
        return _filename;
    };
}
}
