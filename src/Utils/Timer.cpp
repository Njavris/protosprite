#include "Timer.h"

namespace protosprite
{
namespace utils
{
    Timer::Timer()
    {
        _start = glfwGetTime();
        _frames = 0;
    };

    const int Timer::incFrame()
    {
        return ++_frames;
    };

    double Timer::getTime()
    {
        _elapsed = glfwGetTime() - _start;
        return _elapsed;
    };

    int Timer::getFPS()
    {
        return (int)((double)_frames / _elapsed);
    };

    int Timer::getFrames()
    {
        return _frames;
    };

    void Timer::reset()
    {
        _start = glfwGetTime();
        _frames = 0;
    };
}
}
