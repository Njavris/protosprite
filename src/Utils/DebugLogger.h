#pragma once

#include "Defines.h"

#include <time.h>

#ifdef PRINT_LOG
#include <stdio.h>
#endif // PRINT_LOG

//void logMessg(const char* source, const char* msg, int level);

namespace protosprite
{
namespace utils
{

class DebugLogger
{
    int _level;
    const char *_filename;
    static DebugLogger *_instance;
    DebugLogger():_level(0), _filename("debug.log") {};
    DebugLogger(const DebugLogger &cpy);
    void Init();
public:
    static DebugLogger* getInstance();
    void setLevel(int lvl);
    const int getLevel() const;
    void setFilename(const char* filename);
    const char* getFilename() const;
    void logMessage(const char* source, const char* msg, int level);
};
}
}
