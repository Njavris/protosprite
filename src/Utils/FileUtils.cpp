#include "FileUtils.h"

namespace protosprite
{
namespace utils
{
    char* FileUtils::readFile(const char *filePath)
    {
        FILE* file = fopen(filePath, "rt");
        fseek(file, 0, SEEK_END);
        unsigned long length = ftell(file);

        char* data = (char*)calloc(1,length + 1);
        fseek(file, 0, SEEK_SET);
        fread(data, 1, length, file);
        fclose(file);

        return data;
    }
}
}
