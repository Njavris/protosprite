#pragma once

//utils::DebugLogger
#define PRINT_LOG

//graphics
#define RENDERER_ARR_BUFF_ITEMS	        65535

#define SHADER_VERTEX_LAYOUT             0
#define SHADER_TEXTURE_COORD_LAYOUT      1
#define SHADER_TEXTURE_ID_LAYOUT	     2
#define SHADER_TEXTURE_ATLASS_POSITION   3
#define SHADER_COLOR_LAYOUT	             4

#define TEXTURE_ARRAY_SIZE               8

#define LETTER_OFFSET                    -31
