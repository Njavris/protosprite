#pragma once
#include <ctime>
#include <GL\glew.h>
#include <GLFW\glfw3.h>

namespace protosprite
{
namespace utils
{
class Timer
{
    double _start;
    double _elapsed;
    int _frames;
public:
    Timer();
    const int incFrame();
    double getTime();
    int getFPS();
    int getFrames();
    void reset();
};
}
}
