Personal project to learn openGL

Main purpose is to create 2D game framework/engine for simple videogame design

Development IDE: CodeBlocks 16.01
Compiler: mingw32-c++ (tdm-1) 4.9.2
OS: Windows 8.1(x64)

Libraries:
SOIL
GLEW
GLFW

Resources:
Test images are from http://crawl.develz.org/wordpress/
Test bitmap font is from http://forums.nesdev.com/viewtopic.php?t=8440